/*
-----------------------------------------------------------------------------
Filename:    IKElement.h
-----------------------------------------------------------------------------

Define una cadena de huesos preparada para su trabajo con el solucionador
de cinem�tica inversa CCD.
-----------------------------------------------------------------------------
*/
#ifndef __IKElement_h_
#define __IKElement_h_

#include "StdAfx.h"
#include "Ogre.h"


using namespace Ogre;
using namespace std; 


/**
* Almacena en una �nica estructura un conjunto de restricciones de �ngulos de Euler para los 3 grados de libertad (yaw,pitch,roll)
*
* Define �ngulo m�ximo y m�nimo de pitch (x), yaw (y) y roll (z), para su posterior uso a la hora de ejecutar el solucionador CCD con los elementos IKElement.
*
* @author Gabriel Sanmart�n D�az
* @see Personaje.ComputeCCDLink3D()
* @see IKElement
*/
typedef struct
{
	Real max_x;
	Real min_x;
	Real max_y;
	Real min_y;
	Real max_z;
	Real min_z;
} DOF_constraint;


/**
* Agrega sobre un conjunto de huesos informaci�n relativa a la l�gica de cinem�tica inversa sobre el mismo.
*
* Introduce la informaci�n necesaria para el trabajo con el m�todo CCD para implementar soluciones de cinem�tica inversa.
*
* Incluye la lista de huesos involucrados en una cadena de soluci�n IK, indica dentro de la misma el hueso efector y el primer eslab�n de la cadena, e incorpora las restricciones de grados de libertad de movimiento.
*
* @todo Implementar como un wrapper o proxy sobre la clase Bone de Ogre
*
* @author Gabriel Sanmart�n D�az
*/
class IKElement
{

	friend class Personaje;

	public:
		IKElement(String name, SceneNode *sNode);
		IKElement(String name);
		virtual ~IKElement(void);
		//a�ade un Hueso al sistema y aumenta en 1 el numero de huesos
		void addBone(Bone* bone, DOF_constraint dof);
		//Permite cambiar el hueso effector
		void setEffector(int n);
		//Permite cambiar el primer hueso de la cadena
		void setFirstBone(int n);
		//cambia el nodo asociado
		void setNode(SceneNode *sNode);
		//devuelve el nombre del elemento
		String getName();
		//Obtiene un hueso a partir de su �ndice
		Bone* getBone(int idx);

	protected:
		int				numBones;		//n�mero total de huesos en el sistema
		int				posEffector;	//posici�n del efector dentro de la cadena (i.e. �ltimo eslab�n de la misma)
		int				firstBone;		//desde qu� hueso empezamos a trabajar
		String			nombre;			//nombre que queremos dar al sistema;
		DOF_constraint	dofs[6];		//m�ximos �ngulos en cada grado de libertad
		Bone			*bones[6];		//los huesos, m�ximo 6
		SceneNode		*node;
};


#endif // #ifndef __IKElement_h_
