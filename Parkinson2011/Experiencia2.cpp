/*
-----------------------------------------------------------------------------
Filename:    Experiencia2.cpp
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
*/
#include "StdAfx.h"
#include "Experiencia2.h"
#define IR_CAMERA  1
#define KINECT 2

bool emitiendoCero = false;

Vector3 posDer;
Vector3 posIzq;


int method = KINECT;
float diferenciaAltura;

bool tocado = false;
bool saleDiana = true;
float tiempoDeToque = 0.0f;
//-------------------------------------------------------------------------------------
Experiencia2::Experiencia2(int	_protocolos, int _dataPerProtocol, int _expData) :  ExperienciaManos(_protocolos, _dataPerProtocol, _expData), 
	entDianaDerecha(0), 
	entDianaIzquierda(0),
	scnDianaDerecha(0),
	scnDianaIzquierda(0),
	scnDianasComun(0),
	posDianas(0,0,0),
	posDianaDerecha(0,0,0),
	posDianaIzquierda(0,0,0),
	showDianasBoundindBox(false),
	t_ciclo(0.0f),
	velocidadManos(0),
	mDianaEncendida(-1),
	puedeAparecerDiana(false),
	debugSettings(true),
	colisionDebug(true),
	colisiona_diana_izquierda(true),
	colisiona_diana_derecha(true),
	colisionWrite(true),
	colision(false),
	timeToLive(0.0f),
	pointsDebug(false),
	yCeroDerecho(0.0f)
{
	mInfo["Nombre"] = "Experiencia 2";
	mInfo["Description"] = "Captura de movimiento y animaci�n de alcance de dianas.";
	file1 = FileWriter("diana");

	inputPortGUI = 4021;

	for(int i=0;i<nProtocolos;i++){
		ProtocoloExp2 p;
		ordenProtocolos.push_back(i);
		protocolos.push_back(p);
	}

	for(int i=0;i<4;i++){
		puntosFinales[i] = Vector3(0,0,0);
	}
	for(int i=0;i<20;i++)
		puntosDetectados[i] = Vector3(0,0,0);

}
//-------------------------------------------------------------------------------------
Experiencia2::~Experiencia2(void)
{
}
//-------------------------------------------------------------------------------------
void Experiencia2::createScene(void)
{
	/* FIN DEL INIT */
	ExperienciaManos::createScene();

	manDerSphere = mSceneMgr->createEntity("ESFDER", "sphere.mesh");
	manDerSphereNode= mSceneMgr->getRootSceneNode()->createChildSceneNode("ManoDerNode");
	manDerSphereNode->attachObject(manDerSphere);
	manDerSphereNode->scale(0.3f,0.35f,0.8f);
	manDerSphereNode->showBoundingBox(false);
	manDerSphereNode->setVisible(false);

	manIzqSphere = mSceneMgr->createEntity("ESFIZQ", "sphere.mesh");
	manIzqSphereNode= mSceneMgr->getRootSceneNode()->createChildSceneNode("ManoIzNode");
	manIzqSphereNode->attachObject(manIzqSphere);
	manIzqSphereNode->scale(0.3f,0.25f,0.8f);
	manIzqSphereNode->showBoundingBox(false);
	manIzqSphereNode->setVisible(false);


	manDerSphereNode->scale(0.01f,0.01f,0.01f);
	manIzqSphereNode->scale(0.01f,0.01f,0.01f);
	manualObjectCollision = mSceneMgr->createManualObject("manual");
	mSceneMgr->getRootSceneNode()->createChildSceneNode("manual")->attachObject(manualObjectCollision);


	file1.init();
	file1.dumpHeader("click,tiempo");

	fileEventoA.init();
	fileEventoA.dumpHeader("EVENTO,tiempo(ms)");
	fileEventoB.init();
	fileEventoB.dumpHeader("EVENTO,tiempo(ms)");

	posInicialManoDer = personaje->getBoneAbsolutePosition("ManDer");
	posInicialManoEsq = personaje->getBoneAbsolutePosition("ManEsq");
}
//-------------------------------------------------------------------------------------
void Experiencia2::createCamera(void)
{
	ExperienciaManos::createCamera();
	// Position it at 500 in Z direction
	nodeCamera->setPosition(Ogre::Vector3(0.0f, -4.50f, 29.773f));

	nodeCamera->setOrientation(Quaternion(0.993f,-0.117104,-0.00117233, -0.00058272));
	mCamera->setFOVy(Degree(85));
	mCamera->setNearClipDistance(0.7f);
}
//-------------------------------------------------------------------------------------
bool Experiencia2::keyPressed( const OIS::KeyEvent &arg )
{
	ExperienciaManos::keyPressed(arg);
	if(arg.key == OIS::KC_SPACE){
		calibrationDone = !calibrationDone;
	}
	if(arg.key == OIS::KC_F9){
		mCamera->setFOVy(Degree(mCamera->getFOVy()+Radian(Degree(1))));
	}
	if(arg.key == OIS::KC_F10){
		mCamera->setFOVy(Degree(mCamera->getFOVy()-Radian(Degree(1))));
	}	
	if(arg.key == OIS::KC_F3)
	{
		showIKPoints=!showIKPoints;
		posManoDebug=!posManoDebug;
	}


	return true;
}
//-------------------------------------------------------------------------------------
void Experiencia2::initDianas(void)
{
	//Creamos las entidades para las dos dianas
	entDianaDerecha = mSceneMgr->createEntity("DianaDer", "diana.mesh");
	entDianaIzquierda = mSceneMgr->createEntity("DianaEsq","diana.mesh");

	//Creamos un nodo para albergas las dos dianas, en este nodo metemos los dos nodos individuales de cada una
	//Esto es �til para mover las dos dianas a la vez o solo una.
	scnDianasComun = mSceneMgr->getRootSceneNode()->createChildSceneNode("DianasNode");

	posDianas = Vector3(0.0f,-5.4f,26.1f);
	posDianaDerecha = Vector3(0.8f,0.0f,0.0f);
	posDianaIzquierda = Vector3(-0.8f,0.0f,0.0f);
	


	scnDianasComun->setPosition(posDianas);

	//Metemos la diana derecha
	scnDianaDerecha = scnDianasComun->createChildSceneNode(posDianaDerecha);
	scnDianaDerecha->scale(1.5,1.5,1.5);
	scnDianaDerecha->attachObject(entDianaDerecha);

	//Metemos la diana izquierda
	scnDianaIzquierda = scnDianasComun->createChildSceneNode(posDianaIzquierda);
	scnDianaIzquierda->attachObject(entDianaIzquierda);
	scnDianaIzquierda->scale(1.5,1.5,1.5);



	Vector3 posPunto = Vector3(-0.0,0.0f,0.0f);
	Entity *entPunto = mSceneMgr->createEntity("PuntoNegro", "diana.mesh");
	entPunto->setMaterialName("black");
	SceneNode *scnPunto = scnDianasComun->createChildSceneNode(posPunto);
	scnPunto->attachObject(entPunto);
	scnPunto->scale(0.2f, 0.2f, 0.2f);


	scnDianaDerecha->showBoundingBox(false);
	scnDianaIzquierda->showBoundingBox(false);
}
//-------------------------------------------------------------------------------------�
void Experiencia2::createScenario(void)
{
	/* FIN DEL INIT */
	ExperienciaManos::createScenario();
	initDianas();	
}
//-------------------------------------------------------------------------------------
void Experiencia2::setManuallyControlled(bool set){


	//Vamos a obtener los huesos que usaremos, dedos, manos, codos, hombros
	Bone* dedo2Der01 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("Dedo2Der01");
	Bone* dedo2Der02 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("Dedo2Der02");
	Bone* dedo2Der03 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("Dedo2Der03");
	Bone* dedo2Der04 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("Dedo2Der04");
	Bone* dedo2Esq01 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("Dedo2Esq01");
	Bone* dedo2Esq02 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("Dedo2Esq02");
	Bone* dedo2Esq03 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("Dedo2Esq03");
	Bone* dedo2Esq04 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("Dedo2Esq04");
	Bone* manDer	 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("ManDer");
	Bone* manEsq	 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("ManEsq");
	Bone* codoDer	 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("CodoDer");
	Bone* codoEsq	 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("CodoEsq");
	Bone* hombroDer	 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("HombroDer");
	Bone* hombroEsq	 = mSceneMgr->getEntity("Personaje")->getSkeleton()->getBone("HombroEsq");


	dedo2Der01->setManuallyControlled(set);
	dedo2Der02->setManuallyControlled(set);
	dedo2Der03->setManuallyControlled(set);
	dedo2Der01->setManuallyControlled(set);
	dedo2Der04->setManuallyControlled(set);
	dedo2Esq01->setManuallyControlled(set);
	dedo2Esq02->setManuallyControlled(set);
	dedo2Esq03->setManuallyControlled(set);
	dedo2Esq04->setManuallyControlled(set);
	manDer->setManuallyControlled(set);
	codoDer->setManuallyControlled(set);
	hombroDer->setManuallyControlled(set);
	manEsq->setManuallyControlled(set);
	codoEsq->setManuallyControlled(set);
	hombroEsq->setManuallyControlled(set);
}
//-------------------------------------------------------------------------------------
void Experiencia2::updateVars(void){

	started	= datosGUI[0]!=0;
	/// Los tres primeros elementos del buffer \c datosGUI de recepci�n contienen los valores comunes. A partir de entonces
	/// se agrupan los valores de los N protocolos, de forma secuencial, 14 valores por protocolo
	random				= datosGUI[1]!=0;
	resetExp			= datosGUI[2]!=0;

	for(int i=0;i<nProtocolos;i++){
		int indice = (i*nDataPerProtocol) + nExpData;
		

		protocolos[i].Invertir				= datosGUI[indice + 0]!=0;
		protocolos[i].IsActive				= datosGUI[indice + 1]!=0; 
		protocolos[i].EnNegro				= datosGUI[indice + 2]!=0;
		
		protocolos[i].CiclosEntrePulso		= datosGUI[indice + 3];
		protocolos[i].PrioridadCiclos		= datosGUI[indice + 4]!=0;
		protocolos[i].TimeNextProtocol		= datosGUI[indice + 5];
		protocolos[i].CyclesNextProtocol	= datosGUI[indice + 6];

		protocolos[i].ActiveAnim			= datosGUI[indice + 7];
		protocolos[i].ActiveIllum			= datosGUI[indice + 8];
		protocolos[i].AutoAnim				= datosGUI[indice + 9]!=0;
		protocolos[i].TimeToShowTarget		= datosGUI[indice + 10];
		protocolos[i].TimeToStartAnimation	= datosGUI[indice + 11];
		protocolos[i].TimeToEndAnimation	= datosGUI[indice + 12];
		protocolos[i].ExtraWaitingTime		= datosGUI[indice + 13];
		protocolos[i].DistanceBwnTargets	= datosGUI[indice + 14];
		protocolos[i].ShouldLightenOnTouch  = datosGUI[indice + 15];
		protocolos[i].TimeToLightenTarget   = datosGUI[indice + 16];
		protocolos[i].TimeToStopLighten		= datosGUI[indice + 17];
	}

}
//-------------------------------------------------------------------------------------
void Experiencia2::detectaColisiones(ProtocoloExp2 protocol){
	string texto = "";
	bool nada_en_primero = false;
	bool colisionDerecha = false;

	bool allowColisionRight = (isDianaDerechaEncendida()); //&& protocol.AutoAnim == true;
	bool allowColisionLeft = (isDianaIzquierdaEncendida()); //&& protocol.AutoAnim == true;

	if(scnDianaDerecha->_getWorldAABB().intersects(manDerSphereNode->_getWorldAABB()) && allowColisionRight){

		if(colisionDebug){
			printLine("COLISION DCHA:","Panel dcho, mano dcha");
		}
		if(protocol.ShouldLightenOnTouch){
			iluminaDiana(DIANA_DERECHA,   DIANA_ENCENDIDA);	   
			iluminaDiana(DIANA_IZQUIERDA, DIANA_APAGADA);
		}

		scnDianaDerecha->setPosition(
			scnDianaDerecha->getPosition().x,
			scnDianaDerecha->getPosition().y,
			(manDerSphereNode->_getWorldAABB().getCorner(Ogre::AxisAlignedBox::FAR_LEFT_TOP) - scnDianasComun->getPosition()).z + 0.2
			);

		colisionDerecha = true;
		if(started && colisionWrite && entDianaDerecha->isVisible()) {
			colisionWrite = false;
			colisiona_diana_derecha   = true;
			file1.dumpDataTime(getEllapsedMillisecs(),"[MANO DERECHA TOCA PANEL DERECHO],"); 
			/**
			PULSO 5V EN EL CONTACTO CON LA DIANA
			**/
			pulseController.emitPulse(1, 5);
			emitiendoCero = false;
		}
	}
	if(scnDianaIzquierda->_getWorldAABB().intersects(manDerSphereNode->_getWorldAABB()) && allowColisionLeft){
		if(protocol.ShouldLightenOnTouch){
			iluminaDiana(DIANA_DERECHA,   DIANA_APAGADA);	   
			iluminaDiana(DIANA_IZQUIERDA, DIANA_ENCENDIDA);
		}

		scnDianaIzquierda->setPosition(
			scnDianaIzquierda->getPosition().x,
			scnDianaIzquierda->getPosition().y,
			(manDerSphereNode->_getWorldAABB().getCorner(Ogre::AxisAlignedBox::FAR_LEFT_TOP) - scnDianasComun->getPosition()).z
			);
		colisionDerecha = true;
		if(started && colisionWrite && entDianaIzquierda->isVisible()) {
			/**
			PULSO 5V EN EL CONTACTO CON LA DIANA
			**/
			pulseController.emitPulse(1, 5);
			emitiendoCero = false;
			colisionWrite = false;
			colisiona_diana_izquierda = true; 
			file1.dumpDataTime(getEllapsedMillisecs(),"[MANO DERECHA TOCA PANEL IZQUIERDO],"); 
		}
		if(colisionDebug){
			mDebugLineNames.push_back("COLISIONa DCHA:");
			mDebugLineValues.push_back("Panel izdo, mano dcha");
		}
	}


	if(!colisionDerecha) {
		nada_en_primero = true;
		if(colisionDebug){
			mDebugLineNames.push_back("COLISIONd DCHA:");
			mDebugLineValues.push_back("No colisi�n");
		}
	}

	if(colisionDebug){
		mDebugLineNames.push_back("COLISION IZDA:");
	}
	bool colisionIzquierda = false;
	if(scnDianaDerecha->_getWorldAABB().intersects(manIzqSphereNode->_getWorldAABB()) && allowColisionRight){
		if(colisionDebug){
			mDebugLineValues.push_back("Panel dcho, mano izda");
		}
		if(protocol.ShouldLightenOnTouch){
			iluminaDiana(DIANA_DERECHA,   DIANA_ENCENDIDA);	   
			iluminaDiana(DIANA_IZQUIERDA, DIANA_APAGADA);
		}

		colisionIzquierda = true;
		scnDianaDerecha->setPosition(
			scnDianaDerecha->getPosition().x,
			scnDianaDerecha->getPosition().y,
			(manIzqSphereNode->_getWorldAABB().getCorner(Ogre::AxisAlignedBox::FAR_LEFT_TOP) - scnDianasComun->getPosition()).z /*+ 0.1*/
			);
		if(started && colisionWrite && entDianaDerecha->isVisible()) {
			colisiona_diana_derecha   = true;
			colisionWrite = false;
			/**
			PULSO 5V EN EL CONTACTO CON LA DIANA
			**/
			pulseController.emitPulse(1, 5);
			emitiendoCero = false;
			file1.dumpDataTime(getEllapsedMillisecs(),"[MANO IZQUIERDA TOCA PANEL DERECHO],"); 
		}
	}
	if (scnDianaIzquierda->_getWorldAABB().intersects(manIzqSphereNode->_getWorldAABB()) && allowColisionLeft){

		if(!entDianaDerecha->isVisible()){
			if(protocol.ShouldLightenOnTouch){
				iluminaDiana(DIANA_DERECHA,   DIANA_APAGADA);	   
				iluminaDiana(DIANA_IZQUIERDA, DIANA_ENCENDIDA);
			}
		}
		colisionIzquierda = true;
		scnDianaIzquierda->setPosition(
			scnDianaIzquierda->getPosition().x,
			scnDianaIzquierda->getPosition().y,
			(manIzqSphereNode->_getWorldAABB().getCorner(Ogre::AxisAlignedBox::FAR_LEFT_TOP) - scnDianasComun->getPosition()).z
			);
		if(started && colisionWrite && entDianaIzquierda->isVisible()) {
			colisiona_diana_izquierda = true; 
			colisionWrite = false;
			/**
			PULSO 5V EN EL CONTACTO CON LA DIANA POR EL CANAL 1
			**/
			pulseController.emitPulse(1, 5);
			emitiendoCero = false;
			file1.dumpDataTime(getEllapsedMillisecs(),"[MANO IZQUIERDA TOCA PANEL IZQUIERDO],"); 
		}
		if(colisionDebug){
			mDebugLineValues.push_back("Panel izdo, mano izda");
		}
	}
	if(!colisionIzquierda) {
		if(nada_en_primero){
			if(protocol.ShouldLightenOnTouch){
				iluminaDiana(DIANA_DERECHA,   DIANA_APAGADA);	   		
				iluminaDiana(DIANA_IZQUIERDA, DIANA_APAGADA);
			}
		}
		if(colisionDebug){
			mDebugLineValues.push_back("No colisi�n");
		}
	}
	if(colisionDebug){

		mDebugLineNames.push_back("Pos mano dcha:");
		mDebugLineValues.push_back(StringConverter::toString(manDerSphereNode->getPosition()));
		mDebugLineNames.push_back("Pos mano izda: ");
		mDebugLineValues.push_back(StringConverter::toString(manIzqSphereNode->getPosition()));

		mDebugLineNames.push_back("Pos diana dcha:");
		mDebugLineValues.push_back(StringConverter::toString(scnDianaDerecha->_getDerivedPosition()));
		mDebugLineNames.push_back("Pos diana izda: ");
		mDebugLineValues.push_back(StringConverter::toString(scnDianaIzquierda->_getDerivedPosition()));

	}

}

bool Experiencia2::isDianaDerechaEncendida()
{
	Ogre::SubEntity* subEnt = entDianaDerecha->getSubEntity(0);
	return subEnt->getMaterialName() == "MDianaEncendida";
}


bool Experiencia2::isDianaIzquierdaEncendida()
{
	Ogre::SubEntity* subEnt = entDianaIzquierda->getSubEntity(0);
	return subEnt->getMaterialName() == "MDianaEncendida";
}

//-------------------------------------------------------------------------------------
void Experiencia2::iluminaDiana(int diana, int estado){
	if(diana == DIANA_DERECHA){
		if(estado == DIANA_ENCENDIDA){
			entDianaDerecha->setMaterialName("MDianaEncendida");
		}
		else if(estado == DIANA_APAGADA) {
			entDianaDerecha->setMaterialName("MDiana"); // podr�a crear otro material que se llamara MDianaApagada o algo as�, pero ser�a solo una paja mental
		}
	}
	else if(diana == DIANA_IZQUIERDA){
		if(estado == DIANA_ENCENDIDA){
			entDianaIzquierda->setMaterialName("MDianaEncendida");
		}
		else if(estado == DIANA_APAGADA) {
			entDianaIzquierda->setMaterialName("MDiana"); 
		}
	}
}
//-------------------------------------------------------------------------------------
void Experiencia2::dumpProtocolData(ProtocoloExp2 p, int i){

	
	String aux;
	if(i>0){
		file1.dumpTextNL("FIN DE PROTOCOLO\n");
		file1.dumpTextNL("----------------------------------------------\n\n");
	}
	file1.dumpTextNL("----------------------------------------------\n");
	aux  = "[PROTOCOLO ";
	aux.append(StringConverter::toString(i+1));
	aux.append(" INICIADO] ");
	

	int elapMilli = getEllapsedMillisecs();
	//los enviamos a imprimir
	file1.dumpDataTime(elapMilli, (char *)aux.c_str());
	file1.dumpTextNL("[DATOS PROTOCOLO]\n");
	file1.dumpTextNL("----------------------------------------------\n");
	aux = "Inversion: ";
	if(p.Invertir)
		aux.append("Si\n");
	else
		aux.append("No\n");
	file1.dumpTextNL((char *)aux.c_str());

	aux  = "Protocolo activo: ";
	if(p.IsActive)
		aux.append("Si\n");
	else
		aux.append("No\n");
	file1.dumpTextNL((char *)aux.c_str());

	aux  = "Protocolo en negro: ";
	if(p.EnNegro)
		aux.append("Si\n");
	else
		aux.append("No\n");
	file1.dumpTextNL((char *)aux.c_str());



	aux = "Diana activa : ";
	switch(p.ActiveIllum){
		case Random:
			aux.append("Aleatorio");
			break;
		case RightPanel:
			aux.append("Diana derecha");
			break;
		case LeftPanel:
			aux.append("Diana Izquierda");
			break;
	}
	aux.append("\n");
	file1.dumpTextNL((char *)aux.c_str());
	
	aux = "Activar animaci�n (desactivar tracking) : ";
	if(p.AutoAnim)
		aux.append("Si\n");
	else
		aux.append("No\n");
	file1.dumpTextNL((char *)aux.c_str());

	aux = "Animaci�n activa: ";
	switch(p.ActiveAnim){
		case DerechoOblicuo:
				aux.append("Derecho oblicuo");
				break;
		case DerechoRecto:
				aux.append("Derecho recto");
				break;
		case IzquierdoOblicuo:
				aux.append("Izquierdo oblicuo");
				break;
		case IzquierdoRecto:
			aux.append("Izquierdo recto");
				break;
	}
	aux.append("\n");
	file1.dumpTextNL((char *)aux.c_str());

	
	aux = "Tiempo para mostrar la diana: ";
	aux.append(StringConverter::toString(p.TimeToShowTarget));	
	aux.append("\n");
	file1.dumpTextNL((char *)aux.c_str());


	aux  = "Tiempo de inicio de animaci�n: ";
	aux.append(StringConverter::toString(p.TimeToStartAnimation));
	aux.append("\n");
	file1.dumpTextNL((char *)aux.c_str());
	
	aux  = "Tiempo de finalizaci�n de animaci�n: ";
	aux.append(StringConverter::toString(p.TimeToEndAnimation));
	aux.append("\n");
	file1.dumpTextNL((char *)aux.c_str());

	aux  = "Tiempo de finalizaci�n de ciclo: ";
	aux.append(StringConverter::toString(p.TimeToEndAnimation));
	aux.append("\n");
	file1.dumpTextNL((char *)aux.c_str());
	
	aux  = "Ciclos siguiente protocolo: ";
	aux.append(StringConverter::toString(p.CyclesNextProtocol));
	aux.append("\n");
	file1.dumpTextNL((char *)aux.c_str());

	aux  = "Tiempo siguiente protocolo: ";
	aux.append(StringConverter::toString(p.TimeNextProtocol));
	aux.append("\n");
	file1.dumpTextNL((char *)aux.c_str());

	
	file1.dumpTextNL("----------------------------------------------\n");
	file1.dumpTextNL("INICIO REGISTRO\n");
	file1.dumpTextNL("evento, tiempo\n");
}
//-------------------------------------------------------------------------------------
bool Experiencia2::frameRenderingQueued(const FrameEvent& evt)
{
	//Limpiampos las l�neas de debug y asignamos el comportamiento de debug a los diferentes objetos
	{
		clearDebug();
		puntomanoDer->setVisible(showIKPoints);
		puntomanoIzq->setVisible(showIKPoints);
		puntodedoDer->setVisible(showIKPoints);
		puntodedoIzq->setVisible(showIKPoints);
		IKCubeDerNode->setVisible(showIKPoints);
		showDianasBoundindBox = false;
		debugSettings = true;
		colisionDebug = false;
		pointsDebug = false;
		printLine("FOV", mCamera->getFOVy());
	}
	//SOCKETS
	{
		//actualizamos los buffers de los sockets
		updateSockets();
		if(method == IR_CAMERA)
		{
			numPuntos = datosCamaras[0];
			for(int i=0;i<numPuntos;i++){
				int indice = i*3;

				puntosDetectados[i].x = datosCamaras[indice+1];
				puntosDetectados[i].y = datosCamaras[indice+2];
				puntosDetectados[i].z = datosCamaras[indice+3];
			}

			numPuntosDerecha = datosCamaras[30];

			//printLine("NumpuntosDcha", numPuntosDerecha);
			for(int i=0;i<numPuntosDerecha;i++){
				int indice = (i*3)+30;

				puntosDetectadosDerecha[i].x = datosCamaras[indice+1];
				puntosDetectadosDerecha[i].y = datosCamaras[indice+2];
				puntosDetectadosDerecha[i].z = datosCamaras[indice+3];
			}
			//printLine("PrimerptoDcha", puntosDetectadosDerecha[0]);
			//printLine("UltimoptoDcha", puntosDetectadosDerecha[numPuntosDerecha-1]);
			//printLine("...", "...");
			numPuntosIzquierda = datosCamaras[60];
			//printLine("NumpuntosIzda", numPuntosIzquierda);
			for(int i=0;i<numPuntosIzquierda;i++){
				int indice = (i*3)+60;

				puntosDetectadosIzquierda[i].x = datosCamaras[indice+1];
				puntosDetectadosIzquierda[i].y = datosCamaras[indice+2];
				puntosDetectadosIzquierda[i].z = datosCamaras[indice+3];
			}
			//printLine("PrimerptoIzda", puntosDetectadosIzquierda[0]);
			//printLine("UltimoptoIzda", puntosDetectadosIzquierda[numPuntosIzquierda-1]);
			//printLine("...", "...");
		}
		else{
			puntosDetectados[0].x = datosCamaras[0];
			puntosDetectados[0].y = datosCamaras[1];
			puntosDetectados[0].z = datosCamaras[2];
			puntosDetectados[1].x = datosCamaras[3];
			puntosDetectados[1].y = datosCamaras[4];
			puntosDetectados[1].z = datosCamaras[5];

			printLine("Punto Mano izda", puntosDetectados[0]);
		}

		//S�lo si hemos recibido par�metros de la GUI actualizamos dichas variables
		if(recepcionOK) 
			updateVars();
	}

	//Debug de los par�metros de la escena, para comprobar que todo est� siendo recibido correctamente
	if(debugSettings){
		printLine("Started",started);

		printLine("Protocolo:", indice_protocolo_actual);
		printLine("Activo",protocolos[indice_protocolo_actual].IsActive);
		printLine("Invertir",protocolos[indice_protocolo_actual].Invertir);
		printLine("EnNegro",protocolos[indice_protocolo_actual].EnNegro);

		printLine("ActiveAnim",protocolos[indice_protocolo_actual].ActiveAnim);
		printLine("IllumOptions",protocolos[indice_protocolo_actual].ActiveIllum);
		printLine("Activar animacion",protocolos[indice_protocolo_actual].AutoAnim);

		printLine("TimeToShowTarget",protocolos[indice_protocolo_actual].TimeToShowTarget);
		printLine("TimeToStartAnim:",protocolos[indice_protocolo_actual].TimeToStartAnimation);
		printLine("TimeToEndAnim",protocolos[indice_protocolo_actual].TimeToEndAnimation);
		printLine("TimeCycle",protocolos[indice_protocolo_actual].ExtraWaitingTime);

		printLine("Prioridad ciclos",protocolos[indice_protocolo_actual].PrioridadCiclos);
		printLine("CyclesNextProtocol",protocolos[indice_protocolo_actual].CyclesNextProtocol);
		printLine("TiempoNextProtocol",protocolos[indice_protocolo_actual].TimeNextProtocol);
		printLine("Ciclos Entre pulso",protocolos[indice_protocolo_actual].CiclosEntrePulso);

		printLine("Tiempo ilum dianas",protocolos[indice_protocolo_actual].TimeToLightenTarget);
		printLine("Tiempo stop ilum dianas",protocolos[indice_protocolo_actual].TimeToStopLighten);
		printLine("Iluminar al toque",protocolos[indice_protocolo_actual].ShouldLightenOnTouch);
	}



	manDerSphereNode->setPosition(personaje->getBoneAbsolutePosition("Dedo2Der01"));
	manIzqSphereNode->setPosition(personaje->getBoneAbsolutePosition("Dedo2Esq01"));

	if(pointsDebug){
		printLine("Numpuntos", numPuntos);
		for(int i=0;i<numPuntos;i++){
			printLine(StringConverter::toString(i), puntosDetectados[i]);
		}
	}

	//started=true;
	if(!calibrationDone)
	{
		calibrate();
	}
	else
	{
		if(started){
			executeExperienceFrame(evt);
		}//end if started
	}//end logica calibracion
	//que haga tambi�n lo de la clase madre
	return Experiencia::frameRenderingQueued(evt);
}
//-------------------------------------------------------------------------------------
void Experiencia2::calibrate(void){
	//Asignar
	puntosFinales[0] = puntosDetectados[0]; //Manizq
	puntosFinales[1] = puntosDetectados[1]; //dedoIzq

	puntosFinales[2] = puntosDetectados[2]; //ManDer
	puntosFinales[3] = puntosDetectados[3]; //DedoDer

	yCeroDerecho = puntosFinales[2].z * (3.676/200) + -3.85441;

	yCeroIzquierdo = puntosFinales[0].z * (3.676/200) + -3.85441;


	diferenciaAltura = yCeroIzquierdo - yCeroDerecho;
	//antes usaba esto pero no permitia recalibrar ya que al pulsar espacio tomaba la posicion actual del brazo (si esta arriba se toma esa pos como cero)
	//posDer = personaje->getBoneAbsolutePosition("ManDer");

	//Asi que uso la posicion inicial de la mano derecha, tomada al crear la escena
	yCeroDerecho = posInicialManoDer.y-yCeroDerecho;


	//Vector3 posIzq = personaje->getBoneAbsolutePosition("ManEsq");
	//Same here
	yCeroIzquierdo = posInicialManoEsq.y-yCeroIzquierdo;

	printLine("Dif altura", diferenciaAltura);

	if(false){
		//Los de la c�mara derecha, primero salen los de m�s a la derecha
		puntosFinalesDerecha[0] = puntosDetectadosDerecha[0]; //ManDer
		puntosFinalesDerecha[1] = puntosDetectadosDerecha[1]; //DedoDer



		yCeroDerecho = puntosFinalesDerecha[1].y * (-3.676/290) + -3.85441;
		Vector3 posDer = personaje->getBoneAbsolutePosition("ManDer");

		yCeroDerecho = posDer.y-yCeroDerecho;


		//Los de la c�mara izquierda, primero salen los de m�s a la derecha
		puntosFinalesIzquierda[0] = puntosDetectadosIzquierda[0]; //ManEsq
		puntosFinalesIzquierda[1] = puntosDetectadosIzquierda[1]; //DedoEsq


		yCeroIzquierdo = puntosFinalesIzquierda[0].y * (-3.676/290) + -3.85441;
		Vector3 posIzq = personaje->getBoneAbsolutePosition("ManEsq");

		yCeroIzquierdo = posIzq.y-yCeroIzquierdo;

	}
}
//-------------------------------------------------------------------------------------
void Experiencia2::executeExperienceFrame(const FrameEvent& evt){

	//Randomizamos la ejecuci�n de protocolos
	if(!randomized && random){
		srand((unsigned)time(0));
		for (int i=0; i<nProtocolos; i++) {
			int r = i + ( rand()  % (nProtocolos-i)); // Random remaining position.
			int temp = ordenProtocolos.at(i); 
			ordenProtocolos.at(i) = ordenProtocolos.at(r); 
			ordenProtocolos.at(r) = temp;
		} //end for
	} //end random
	randomized = true;

	//Reseteamos
	if(resetExp){
		//Al resetear, cosas que hay que hacer
		indice_protocolo_random = 0; //Resetear, evidentemente, el protocolo en ejecuci�n
		t_protocolo = 0; //Resetear el tiempo transcurrido
		t_ciclo = 0;
		numCiclosTranscurridos = 0; //as� como los ciclos transcurridos
		numCiclosEsperados = 0;
		resetExp = 0; //se�al de reseteo
		reiniciarProtocolos = false; 
		primeraVezProtocolo = true; //Hay que indicar que se empieza un nuevo protocolo, para que lo imprima a archivo
	}//end reset

	indice_protocolo_actual= ordenProtocolos.at(indice_protocolo_random);

	//NECESITAMOS LAS CONDICIONES DE FINALIZACION DE UN PROTOCOLO
	if((
		(protocolos[indice_protocolo_actual].PrioridadCiclos && numCiclosTranscurridos < protocolos[indice_protocolo_actual].CyclesNextProtocol )
		|| 
		(!protocolos[indice_protocolo_actual].PrioridadCiclos && t_protocolo < protocolos[indice_protocolo_actual].TimeNextProtocol ))
		&& indice_protocolo_random<nProtocolos 
		&& protocolos[indice_protocolo_actual].IsActive
		)
	{
		executeProtocol(evt);
	} //end if protocolos
	//si no, reseteamos
	else
	{
		//Reseteamos todos los tiempos
		t_protocolo = 0.0f;
		t_ciclo = 0.0f;
		numCiclosTranscurridos = 0;
		numCiclosEsperados = 0;

		if(indice_protocolo_random<nProtocolos - 1){
			indice_protocolo_random++;
		}
		else  {
			indice_protocolo_random = 0;
		}
		primeraVezProtocolo = true;
	} //end else protocolos
	

	if(method == IR_CAMERA){
		solveIKOldMethod(evt);
	}
	else {
		solveIK(evt);
	}

}
//-------------------------------------------------------------------------------------
void Experiencia2::executeProtocol(const FrameEvent& evt){
	
	int alea;

	/**
	PULSO DE 0 V v�a SALIDA 0
	**/
	//emitimos un pulso de cero v para que no siga emitiendo el de 5
	if(!emitiendoCero){
		pulseController.emitPulse(0, 0);
		emitiendoCero = true;
	}
	//Coloca las dianas
	float distancia = protocolos[indice_protocolo_actual].DistanceBwnTargets/100;
	//Metemos la diana derecha
	scnDianaDerecha->setPosition(distancia,0.0f,0.0f);
	scnDianaIzquierda->setPosition(-distancia,0.0f,0.0f);


	////Si es la primera vez que se inicia este protocolo imprimimos toda la configuraci�n del mismo
	if(primeraVezProtocolo){
		primeraVezProtocolo = false;
		puedeAparecerDiana = true;
		dumpProtocolData(protocolos[indice_protocolo_actual],indice_protocolo_actual);	

		//Enviamos el protocolo en ejecucion actual al launcher para que sepa en donde estamos
		float indiceFloat[1];
		indiceFloat[0] = indice_protocolo_actual;
		//dummy value
		indiceFloat[1] = -1;
		//Enviamos el num experiencia actual
		sender->setBuffer((void *)indiceFloat, sizeof(float)*2);
		sender->sync();
		saleDiana = true;

		//La velocidad es, en la animaci�n, por defecto, 1 s. As� que calculamso el tiempo q va a tardar y dividimos 1 entre ese tiempo
		velocidadManos = 2 /  (protocolos[indice_protocolo_actual].TimeToEndAnimation - protocolos[indice_protocolo_actual].TimeToStartAnimation);
	} //end primera vez

	if(protocolos[indice_protocolo_actual].EnNegro){
		setBlankScreen(true);
	}
	else
	{
		setBlankScreen(blackSwitch);
	}
	
	//PULSO 5V por SALIDA 0 AL INICIO DE CICLO
	//Emitimos pulso al inicio de ciclo
	if(t_ciclo==0.0f){
		pulseController.emitPulse(0, 5);
		emitiendoCero=false;
		saleDiana=true;
		mDianaEncendida = NINGUNA_DIANA;
		iluminaDiana(DIANA_DERECHA,   DIANA_APAGADA);	   
		iluminaDiana(DIANA_IZQUIERDA, DIANA_APAGADA);
	}

	//AHORA TODAS LAS VARIABLES DE TIEMPO TIENEN LA MISMA REFERENCIA TEMPORAL
	//ASI QUE LAS ACTUALIZAMOS TODAS Y SIMPLEMENTE CAMBIAMOS EL MOMENTO DEL RESETEO
	//Avanzamos el tiempo y ocnfiguramos el porcentaje de animacion
	t_protocolo += evt.timeSinceLastFrame;
	//Avanzamos el tiempo de evoluci�n del ciclo.
 	t_ciclo += evt.timeSinceLastFrame;
	//t_protocolo = t_ciclo;
	//--------------------------------------------------------L�GICA----------------------------------
	//Si no est�n activas las animaciones las desactivamos expl�citamente y las reseteamos, ponemos los huesos en control manual
	if(!protocolos[indice_protocolo_actual].AutoAnim){
		//desactivamos anims
		as_dianaRectoDta->setEnabled(false);
		as_dianaRectoDta->setTimePosition(0);
		as_dianaCruceDta->setEnabled(false);
		as_dianaCruceDta->setTimePosition(0);
		as_dianaRectoEsq->setEnabled(false);
		as_dianaRectoEsq->setTimePosition(0);
		as_dianaCruceEsq->setEnabled(false);
		as_dianaCruceEsq->setTimePosition(0);
		//activamos el control manual
		setManuallyControlled(true);
	}
	else {
		setManuallyControlled(false);
		//elegimos la animacion activada
		if(protocolos[indice_protocolo_actual].ActiveAnim==DerechoRecto){
			as_currentDiana = as_dianaRectoDta;

			as_dianaCruceDta->setEnabled(false);
			as_dianaCruceDta->setTimePosition(0);
			as_dianaRectoEsq->setEnabled(false);
			as_dianaRectoEsq->setTimePosition(0);
			as_dianaCruceEsq->setEnabled(false);
			as_dianaCruceEsq->setTimePosition(0);
		}
		else if(protocolos[indice_protocolo_actual].ActiveAnim==DerechoOblicuo){
			as_currentDiana = as_dianaCruceDta;

			as_dianaRectoDta->setEnabled(false);
			as_dianaRectoDta->setTimePosition(0);
			as_dianaRectoEsq->setEnabled(false);
			as_dianaRectoEsq->setTimePosition(0);
			as_dianaCruceEsq->setEnabled(false);
			as_dianaCruceEsq->setTimePosition(0);
		}
		else if(protocolos[indice_protocolo_actual].ActiveAnim==IzquierdoRecto){
			as_currentDiana = as_dianaRectoEsq;

			as_dianaRectoDta->setEnabled(false);
			as_dianaRectoDta->setTimePosition(0);
			as_dianaCruceDta->setEnabled(false);
			as_dianaCruceDta->setTimePosition(0);
			as_dianaCruceEsq->setEnabled(false);
			as_dianaCruceEsq->setTimePosition(0);
		}
		else if(protocolos[indice_protocolo_actual].ActiveAnim==IzquierdoOblicuo){
			as_currentDiana = as_dianaCruceEsq;

			as_dianaRectoDta->setEnabled(false);
			as_dianaRectoDta->setTimePosition(0);
			as_dianaCruceDta->setEnabled(false);
			as_dianaCruceDta->setTimePosition(0);
			as_dianaRectoEsq->setEnabled(false);
			as_dianaRectoEsq->setTimePosition(0);
		} //end switch

		as_currentDiana->setEnabled(true);
		as_currentDiana->setLoop(false);
		if(t_ciclo>=protocolos[indice_protocolo_actual].TimeToStartAnimation && t_ciclo<protocolos[indice_protocolo_actual].TimeToEndAnimation)
			as_currentDiana->addTime(velocidadManos*evt.timeSinceLastFrame);
		else
			as_currentDiana->setTimePosition(0);
		
	} //else if manosAuto

	//Ilumina dianas
	{
		//El modo que no es iluminar al toque debe sacar siempre la diana
		if(!protocolos[indice_protocolo_actual].ShouldLightenOnTouch)
		{
			mDianaEncendida = BOTH_DIANAS;
		}

		/** APARECER DIANAS **/
		if(mDianaEncendida == NINGUNA_DIANA && (t_ciclo >= protocolos[indice_protocolo_actual].TimeToShowTarget) && (puedeAparecerDiana)){ // antes era 1.5, ahora adquiero el valor de tiempo desde la gui 
			if(saleDiana)
			{
				/**
				PULSO 5V POR EL CANAL 1 CUANDO SALE LA DIANA 
				**/
				pulseController.emitPulse(1,5);
				saleDiana = false;
			}
			// iluminar de forma autom�tica y aleatoria las dianas con el paso del tiempo
			if(protocolos[indice_protocolo_actual].ActiveIllum == Random){ 
				// ...selecciona de forma aleatoria una de las dos dianas y la enciende
				alea = rand() % 2;
				if(alea == 0)       mDianaEncendida = DIANA_DERECHA;
				else if(alea == 1)  mDianaEncendida = DIANA_IZQUIERDA;
			}
				
			// iluminar s�lo la diana del lado izquierdo de forma autom�tica con el paso del tiempo
			if(protocolos[indice_protocolo_actual].ActiveIllum == LeftPanel){
				mDianaEncendida = DIANA_IZQUIERDA;
			}
			// iluminar s�lo la diana del lado derecho de forma autom�tica con el paso del tiempo
			if(protocolos[indice_protocolo_actual].ActiveIllum == RightPanel){
				mDianaEncendida = DIANA_DERECHA;
			}
		}

		/**El de arriba es para APARECER dianas, ahora el c�digo para ILUMINAR dianas **/
		if(!protocolos[indice_protocolo_actual].ShouldLightenOnTouch && t_ciclo >= protocolos[indice_protocolo_actual].TimeToLightenTarget){  
			
			
			//ENCENDEMOS AMBAS PORQUE TOTAL NO VAN A ESTAR NUNCA LAS DOS EN PANTALLA Y YA SE APAGAN AL REINICIAR CICLO
			if(protocolos[indice_protocolo_actual].ActiveIllum == LeftPanel){
				iluminaDiana(DIANA_IZQUIERDA,   DIANA_ENCENDIDA);
			}
			/**EMITIMOS PULSO DE ILUMINACION DE DIANA*/
			pulseController.emitPulse(1,5);
			if(protocolos[indice_protocolo_actual].ActiveIllum == RightPanel){
				iluminaDiana(DIANA_DERECHA, DIANA_ENCENDIDA);
			}

			if(protocolos[indice_protocolo_actual].ActiveIllum == Random){ 
				// ...selecciona de forma aleatoria una de las dos dianas y la enciende
				if(saleDiana)
				{
				alea = rand() % 2;
				int queDiana = 0;
				if(alea == 0)       queDiana = DIANA_DERECHA;
				else if(alea == 1)  queDiana = DIANA_IZQUIERDA;
				iluminaDiana(queDiana, DIANA_ENCENDIDA);
				saleDiana = false;
				}
			}

			//Si rebasamos el tiempo maximo apagamos las dos
			if(t_ciclo >= protocolos[indice_protocolo_actual].TimeToStopLighten)
			{
				iluminaDiana(DIANA_DERECHA, DIANA_APAGADA);
				iluminaDiana(DIANA_IZQUIERDA, DIANA_APAGADA);
			}
			

			
		}


		// esto deber�a hacerlo mejor.
		// NUNCA PUEDEN ESTAR LAS DOS DIANAS ENCENDIDAS A LA VEZ (al menos nunca nos dijeron eso...) 
		if(mDianaEncendida == NINGUNA_DIANA){
			entDianaDerecha->setVisible(false);
			entDianaIzquierda->setVisible(false);
			scnDianaDerecha->setPosition(posDianaDerecha);
			scnDianaIzquierda->setPosition(posDianaIzquierda);
		}
		else if(mDianaEncendida == DIANA_DERECHA){
			if(!entDianaDerecha->isVisible())
				file1.dumpDataTime(getEllapsedMillisecs(),"[APARECE DIANA DERECHA],");
			entDianaDerecha->setVisible(true);
			entDianaIzquierda->setVisible(false);
		}
		else if(mDianaEncendida == DIANA_IZQUIERDA){
			if(!entDianaIzquierda->isVisible())
				file1.dumpDataTime(getEllapsedMillisecs(),"[APARECE DIANA IZQUIERDA],");
			entDianaDerecha->setVisible(false);
			entDianaIzquierda->setVisible(true);
		}
		//AHORA SI PUEDEN SALIR LAS DOS DIANAS A LA VEZ
		else if(mDianaEncendida == BOTH_DIANAS){
			entDianaDerecha->setVisible(true);
			entDianaIzquierda->setVisible(true);
		}

	}
	detectaColisiones(protocolos[indice_protocolo_actual]);
	{
		if(colisiona_diana_derecha && (mDianaEncendida == DIANA_DERECHA)){
			colisiona_diana_derecha = false;
			if(!colision) { 
				timeToLive	= 0.0f;
				colision = true;
				

				tocado = true;
				tiempoDeToque=t_ciclo;
			}
		}
		else if(colisiona_diana_derecha && (mDianaEncendida != DIANA_DERECHA)){
			colisiona_diana_derecha    = false;
		}

		if(colisiona_diana_izquierda && (mDianaEncendida == DIANA_IZQUIERDA)){
			colisiona_diana_izquierda  = false;
			if(!colision) { 
				timeToLive = 0.0f;
				colision = true;
				

				tocado = true;
				tiempoDeToque=t_ciclo;
			}

		}

		else if(colisiona_diana_izquierda && (mDianaEncendida != DIANA_IZQUIERDA)){
			colisiona_diana_izquierda  = false;
		}

		if(colision) {
			timeToLive += secs;
		}

		if(timeToLive>0.5){

			mDianaEncendida = NINGUNA_DIANA; 
			scnDianaDerecha->setPosition(posDianaDerecha);
			scnDianaIzquierda->setPosition(posDianaIzquierda);
			timeToLive = 0.0f;
			puedeAparecerDiana = false;
			colision = false;
			colisionWrite = true;
		}

	}


	//Si est� en animacion, reiniciamos el tiempo del ciclo y pasamos al siguiente ciclo
	if(t_ciclo>=protocolos[indice_protocolo_actual].ExtraWaitingTime && protocolos[indice_protocolo_actual].AutoAnim){
		t_ciclo=0.0f;
		numCiclosTranscurridos++;
		as_currentDiana->setTimePosition(0);
		puedeAparecerDiana = true;
	}
	else if(
		(t_ciclo)>=protocolos[indice_protocolo_actual].ExtraWaitingTime){
			t_ciclo=0.0f;
		tocado = false;
		numCiclosTranscurridos++;
		//indice_protocolo_actual++;
		puedeAparecerDiana = true;
	}

	if(debugSettings){

		printLine("Tiempo transcurrido protocolo",t_protocolo);
		printLine("Tiempo transcurrido ciclo",t_ciclo);
		printLine("Ciclos transcurridos",numCiclosTranscurridos);
	}
}
//-------------------------------------------------------------------------------------
void Experiencia2::solveIKOldMethod(const FrameEvent& evt){
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////// NADA DE ARPUPPET ESTO LO HICE YO xd ////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//C�MARA CENITAL
		{
			//algoritmo de correspondencia temporal
			int		indiceGanador[4];
			float	distanciaGanadora[4];
			for(int i=0;i<4;i++) {
				distanciaGanadora[i] = 100000000000.0f;
				indiceGanador[i] = -1;
			}

			//Comparamos cada punto detectado con los puntos asociados como buenoas en el frame anterior
			for (int i=0;i<numPuntos;i++){
				for(int j=0;j<4;j++){
					float dist = abs(puntosFinales[j].distance(puntosDetectados[i]));
					pares[i].puntoFinal = puntosFinales[j];
					pares[i].indicePFinal =j;
					pares[i].puntoDetectado = puntosDetectados[i];
					pares[i].indicePDetectado = i;
					pares[i].distancia = dist;

					if(dist<distanciaGanadora[j]){
						distanciaGanadora[j] = dist;
						indiceGanador[j]= i;
					}
				}
			}

			//Ahora buiscamos duplicados y elegimos el de menor distancia
			for(int i=0;i<4;i++){
				int j=i+1;
				while(j<4){
					//Si el indice es positivo y adem�s se rep�te( mismo punto ganador para dos puntos finales)
					if(indiceGanador[i]>=0 && indiceGanador[i]==indiceGanador[j]){
						//Escogemos el que tiene menor distancia y al otro le asignamos indice -1
						if(distanciaGanadora[i]<=distanciaGanadora[j]){
							indiceGanador[j]=-1;
						}
						else
						{
							indiceGanador[i]=-1;
						}
					}
					j++;
				}
			}
			//DECISOR
			for(int i=0;i<4;i++){
				if(pointsDebug){
					printLine("PUNTO", i);
					printLine("Indice Ganador",indiceGanador[i]);
					printLine("Dist Ganador",distanciaGanadora[i]);
					printLine("", "");
				}
				if(indiceGanador[i]>=0){
					puntosFinales[i] = puntosDetectados[indiceGanador[i]];
				}
			}
		}

		if(false)
		{
			//C�MARA LATERAL DERECHA
			{
				//algoritmo de correspondencia temporal
				int		indiceGanador[2];
				float	distanciaGanadora[2];
				for(int i=0;i<2;i++) {
					distanciaGanadora[i] = 100000000000.0f;
					indiceGanador[i] = -1;
				}

				//Comparamos cada punto detectado con los puntos asociados como buenoas en el frame anterior
				for (int i=0;i<numPuntosDerecha;i++){
					for(int j=0;j<2;j++){
						float dist = abs(puntosFinalesDerecha[j].distance(puntosDetectadosDerecha[i]));
						pares[i].puntoFinal = puntosFinalesDerecha[j];
						pares[i].indicePFinal =j;
						pares[i].puntoDetectado = puntosDetectadosDerecha[i];
						pares[i].indicePDetectado = i;
						pares[i].distancia = dist;

						if(dist<distanciaGanadora[j]){
							distanciaGanadora[j] = dist;
							indiceGanador[j]= i;
						}
					}
				}

				//Ahora buiscamos duplicados y elegimos el de menor distancia
				for(int i=0;i<2;i++){
					int j=i+1;
					while(j<2){
						//Si el indice es positivo y adem�s se rep�te( mismo punto ganador para dos puntos finales)
						if(indiceGanador[i]>=0 && indiceGanador[i]==indiceGanador[j]){
							//Escogemos el que tiene menor distancia y al otro le asignamos indice -1
							if(distanciaGanadora[i]<=distanciaGanadora[j]){
								indiceGanador[j]=-1;
							}
							else
							{
								indiceGanador[i]=-1;
							}
						}
						j++;
					}
				}
				//DECISOR
				for(int i=0;i<2;i++){
					if(pointsDebug){
						printLine("PUNTO", i);
						printLine("Indice Ganador",indiceGanador[i]);
						printLine("Dist Ganador",distanciaGanadora[i]);
						printLine("", "");
					}
					if(indiceGanador[i]>=0){
						puntosFinalesDerecha[i] = puntosDetectadosDerecha[indiceGanador[i]];
					}
				}
			}


			//C�MARA LATERAL IZUQIERDA
			{
				//algoritmo de correspondencia temporal
				int		indiceGanador[2];
				float	distanciaGanadora[2];
				for(int i=0;i<2;i++) {
					distanciaGanadora[i] = 100000000000.0f;
					indiceGanador[i] = -1;
				}

				//Comparamos cada punto detectado con los puntos asociados como buenoas en el frame anterior
				for (int i=0;i<numPuntosIzquierda;i++){
					for(int j=0;j<2;j++){
						float dist = abs(puntosFinalesIzquierda[j].distance(puntosDetectadosIzquierda[i]));
						pares[i].puntoFinal = puntosFinalesIzquierda[j];
						pares[i].indicePFinal =j;
						pares[i].puntoDetectado = puntosDetectadosIzquierda[i];
						pares[i].indicePDetectado = i;
						pares[i].distancia = dist;

						if(dist<distanciaGanadora[j]){
							distanciaGanadora[j] = dist;
							indiceGanador[j]= i;
						}
					}
				}

				//Ahora buiscamos duplicados y elegimos el de menor distancia
				for(int i=0;i<2;i++){
					int j=i+1;
					while(j<2){
						//Si el indice es positivo y adem�s se rep�te( mismo punto ganador para dos puntos finales)
						if(indiceGanador[i]>=0 && indiceGanador[i]==indiceGanador[j]){
							//Escogemos el que tiene menor distancia y al otro le asignamos indice -1
							if(distanciaGanadora[i]<=distanciaGanadora[j]){
								indiceGanador[j]=-1;
							}
							else
							{
								indiceGanador[i]=-1;
							}
						}
						j++;
					}
				}
				//DECISOR
				for(int i=0;i<2;i++){
					if(pointsDebug){
						printLine("PUNTO", i);
						printLine("Indice Ganador",indiceGanador[i]);
						printLine("Dist Ganador",distanciaGanadora[i]);
						printLine("", "");
					}
					if(indiceGanador[i]>=0){
						puntosFinalesIzquierda[i] = puntosDetectadosIzquierda[indiceGanador[i]];
					}
				}
			}
		}

		posIKManoIzquierda.x = puntosFinales[0].x * (-4.5/355) + 1.5;
		posIKManoIzquierda.z = puntosFinales[0].y * (-2.5/290) + 28;


		posIKManoIzquierda.y   = (puntosFinales[0].z) * (3.676/200) + -3.85441;
		posIKManoIzquierda.y   += yCeroIzquierdo;
		//posIKManoIzquierda.y   += diferenciaAltura;

		//if(posIKManoIzquierda.y<-7)
		//posIKManoIzquierda.y=-7;

		posIKManoDerecha.x   = puntosFinales[3].x * (-4.5/355) + 3;
		posIKManoDerecha.z   = puntosFinales[3].y * (-2.5/290) + 28;


		posIKManoDerecha.y   = (puntosFinales[3].z) * (3.676/200) + -3.85441;
		//posIKManoDerecha.y	 += yCeroDerecho;
		posIKManoDerecha.y	 += yCeroIzquierdo;

		//posIKManoDerecha.y   *= 1.34f;


		//if(posIKManoDerecha.y<-7)
		//posIKManoDerecha.y=-7;

		posIKDedoIzquierdo.x = puntosFinales[1].x * (-4.5/355) + 1.5;
		posIKDedoIzquierdo.z = puntosFinales[1].y * (-2.5/290) + 28;

		posIKDedoDerecho.x   = puntosFinales[2].x * (-4.5/355) + 3;
		posIKDedoDerecho.z   = puntosFinales[2].y * (-2.5/290) + 28;

		printLine("IK MANDER",posIKManoDerecha);
		printLine("IK MANESQ",posIKManoIzquierda);


		//posIKManoIzquierda.y = personaje->getBoneAbsolutePosition("ManEsq").y;
		//posIKManoDerecha.y  = personaje->getBoneAbsolutePosition("ManDer").y;

		//posIKDedoIzquierdo.y = personaje->getBoneAbsolutePosition("Dedo2Esq01").y;
		//posIKDedoDerecho.y  = personaje->getBoneAbsolutePosition("Dedo2Der01").y;


		puntoManoDerNode->setPosition(posIKManoDerecha);
		puntoManoIzqNode->setPosition(posIKManoIzquierda);

		/* EXP 2 */
		if(!protocolos[indice_protocolo_actual].AutoAnim && started){

			Vector3 position_mano1 = personaje->getBoneAbsolutePosition("ManDer");
			Vector3 position_mano2 = personaje->getBoneAbsolutePosition("ManEsq");

			if(posManoDebug){
				printLine("PTO IK",posIKManoDerecha);
				printLine("MANO D",position_mano1);
				printLine("PTO IK2",posIKManoIzquierda);
				printLine("MANO I ",position_mano2);
			}

			if(!protocolos[indice_protocolo_actual].Invertir){
				//if(t_manoIz>1.0)
				personaje->ComputeCCDLink3D(posIKManoDerecha, "brazoDerecho", 50);
				//if(t_manoDer>1.0)
				personaje->ComputeCCDLink3D(posIKManoIzquierda, "brazoIzquierdo", 50);
			}
			else{
				Bone* codoEsq = personaje->getBone("CodoEsq");
				Bone* hombroEsq = personaje->getBone("HombroEsq");
				Bone* hombroDer = personaje->getBone("HombroDer");
				Bone* codoDer = personaje->getBone("CodoDer");
				personaje->extractAnglesFromBone(hombroEsq, &old_angulos_hombro_izq);
				personaje->extractAnglesFromBone(codoEsq, &old_angulos_codo_izq);

				personaje->extractAnglesFromBone(hombroDer, &old_angulos_hombro_der);
				personaje->extractAnglesFromBone(codoDer, &old_angulos_codo_der);


				personaje->setAngles(hombroDer, &angulos_hombro_der);
				personaje->setAngles(codoDer, &angulos_codo_der);
				personaje->ComputeCCDLink3D(posIKManoDerecha, "brazoDerecho", 50);

				personaje->setAngles(hombroEsq, &angulos_hombro_izq);
				personaje->setAngles(codoEsq, &angulos_codo_izq);
				personaje->ComputeCCDLink3D(posIKManoIzquierda, "brazoIzquierdo", 50);


				personaje->extractAnglesFromBone(hombroEsq, &angulos_hombro_izq);
				personaje->extractAnglesFromBone(codoEsq, &angulos_codo_izq);

				personaje->extractAnglesFromBone(hombroDer, &angulos_hombro_der);
				personaje->extractAnglesFromBone(codoDer, &angulos_codo_der);


			} //end else invertir
		}//end if manosauto
}
//-------------------------------------------------------------------------------------
void Experiencia2::solveIK(const FrameEvent& evt){
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////// NADA DE ARPUPPET ESTO LO HICE YO xd ////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	puntosFinales[0] = puntosDetectados[0];
	puntosFinales[1] = puntosDetectados[1];


	printLine("Punto ManIzq final:", puntosFinales[0]);

	posIKManoIzquierda.x = puntosFinales[0].x * (-4.5/640) + 1.5;
	posIKManoIzquierda.z = puntosFinales[0].y * (-2.5/480) + 28;
	posIKManoIzquierda.y = (puntosFinales[0].z - 420) * (3.676/-340) + -3.85441;

			
	posIKManoDerecha.x   = puntosFinales[1].x * (-4.5/640) + 3;
	posIKManoDerecha.z   = puntosFinales[1].y * (-2.5/480) + 28;
	posIKManoDerecha.y   = (puntosFinales[1].z - 420) * (3.676/-340) + -3.85441;

	printLine("IK MANESQ",posIKManoIzquierda);

	puntoManoDerNode->setPosition(posIKManoDerecha);
	puntoManoIzqNode->setPosition(posIKManoIzquierda);

	/* EXP 2 */

	//if(!exp2manosAuto && started){
	if(true){
		Vector3 position_mano1 = personaje->getBoneAbsolutePosition("ManDer");
		Vector3 position_mano2 = personaje->getBoneAbsolutePosition("ManEsq");

		if(posManoDebug){
			printLine("PTO IK",posIKManoDerecha);
			printLine("MANO D",position_mano1);
			printLine("PTO IK2",posIKManoIzquierda);
			printLine("MANO I ",position_mano2);
		}
				
		if(!protocolos[indice_protocolo_actual].Invertir){
			//if(t_manoIz>1.0)
				personaje->ComputeCCDLink3D(posIKManoDerecha, "brazoDerecho", 50);
			//if(t_manoDer>1.0)
				personaje->ComputeCCDLink3D(posIKManoIzquierda, "brazoIzquierdo", 50);
		}
		else{
			Bone* codoEsq = personaje->getBone("CodoEsq");
			Bone* hombroEsq = personaje->getBone("HombroEsq");
			Bone* hombroDer = personaje->getBone("HombroDer");
			Bone* codoDer = personaje->getBone("CodoDer");
			personaje->extractAnglesFromBone(hombroEsq, &old_angulos_hombro_izq);
			personaje->extractAnglesFromBone(codoEsq, &old_angulos_codo_izq);
			
			personaje->extractAnglesFromBone(hombroDer, &old_angulos_hombro_der);
			personaje->extractAnglesFromBone(codoDer, &old_angulos_codo_der);


			personaje->setAngles(hombroDer, &angulos_hombro_der);
			personaje->setAngles(codoDer, &angulos_codo_der);
			personaje->ComputeCCDLink3D(posIKManoDerecha, "brazoDerecho", 50);

			personaje->setAngles(hombroEsq, &angulos_hombro_izq);
			personaje->setAngles(codoEsq, &angulos_codo_izq);
			personaje->ComputeCCDLink3D(posIKManoIzquierda, "brazoIzquierdo", 50);


			personaje->extractAnglesFromBone(hombroEsq, &angulos_hombro_izq);
			personaje->extractAnglesFromBone(codoEsq, &angulos_codo_izq);

			personaje->extractAnglesFromBone(hombroDer, &angulos_hombro_der);
			personaje->extractAnglesFromBone(codoDer, &angulos_codo_der);


		} //end else invertir
	}//end if manosauto
}