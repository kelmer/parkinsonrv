/*
-----------------------------------------------------------------------------
Filename:    MyCameraMan.cpp
-----------------------------------------------------------------------------
*/

#include "StdAfx.h"
#include "MyCameraMan.h"

MyCameraMan::MyCameraMan(Ogre::Camera* cam)
	:SdkCameraMan(cam),
		multMouseY(1),
		multMouseX(1),
		applyMultiplier(true)
{

}

void MyCameraMan::injectKeyUp(const OIS::KeyEvent& evt){
	//OgreBites::SdkCameraMan::injectKeyDown(evt);
	
	if (evt.key == OIS::KC_W) mGoingForward = false;
	else if (evt.key == OIS::KC_S) mGoingBack = false;
	else if (evt.key == OIS::KC_A) mGoingLeft = false;
	else if (evt.key == OIS::KC_D) mGoingRight = false;
	else if (evt.key == OIS::KC_LSHIFT) mFastMove = false;
	else if (evt.key == OIS::KC_Q) mGoingUp = false;
	else if (evt.key == OIS::KC_E) mGoingDown = false;
}
void MyCameraMan::injectKeyDown(const OIS::KeyEvent& evt){
	//OgreBites::SdkCameraMan::injectKeyUp(evt);
	if (evt.key == OIS::KC_W) mGoingForward = true;
	else if (evt.key == OIS::KC_S) mGoingBack = true;
	else if (evt.key == OIS::KC_A) mGoingLeft = true;
	else if (evt.key == OIS::KC_D) mGoingRight = true;
	else if (evt.key == OIS::KC_LSHIFT) mFastMove = true;
	else if (evt.key == OIS::KC_Q) mGoingUp = true;
	else if (evt.key == OIS::KC_E) mGoingDown = true;
}

#if OGRE_PLATFORM == OGRE_PLATFORM_IPHONE
void MyCameraMan::injectMouseMove(const OIS::MultiTouchEvent& evt)
#else
void MyCameraMan::injectMouseMove(const OIS::MouseEvent& evt)
#endif
	{
		if (mStyle == OgreBites::CS_ORBIT)
		{
			Ogre::Real dist = (mCamera->getPosition() - mTarget->_getDerivedPosition()).length();

			if (mOrbiting)   // yaw around the target, and pitch locally
			{
				mCamera->setPosition(mTarget->_getDerivedPosition());

				mCamera->yaw(Ogre::Degree(-evt.state.X.rel * 0.25f));
				mCamera->pitch(Ogre::Degree(-evt.state.Y.rel * 0.25f));

				mCamera->moveRelative(Ogre::Vector3(0, 0, dist));

				// don't let the camera go over the top or around the bottom of the target
			}
			else if (mZooming)  // move the camera toward or away from the target
			{
				// the further the camera is, the faster it moves
				mCamera->moveRelative(Ogre::Vector3(0, 0, evt.state.Y.rel * 0.004f * dist));
			}
			else if (evt.state.Z.rel != 0)  // move the camera toward or away from the target
			{
				// the further the camera is, the faster it moves
				mCamera->moveRelative(Ogre::Vector3(0, 0, -evt.state.Z.rel * 0.0008f * dist));
			}
		}
		else if (mStyle == OgreBites::CS_FREELOOK)
		{
			if(applyMultiplier){
				mCamera->yaw(Ogre::Degree(-evt.state.X.rel * 0.15f*multMouseY));
				mCamera->pitch(Ogre::Degree(-evt.state.Y.rel * 0.15f*multMouseX));
			}
			else{
				mCamera->yaw(Ogre::Degree(-evt.state.X.rel * 0.15f));
				mCamera->pitch(Ogre::Degree(-evt.state.Y.rel * 0.15f));
			}
		}
	}