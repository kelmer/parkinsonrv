/*
-----------------------------------------------------------------------------
Filename:    Personaje.cpp
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
*/
#include "StdAfx.h"
#include "Personaje.h"

//-------------------------------------------------------------------------------------
Personaje::Personaje(SceneManager* scnMgr, String nombreEntity, String nombreNode, String uri)
{
	//creamos la entidad con el nombre y la ruta al archivo
	entPersonaje = scnMgr->createEntity(nombreEntity, uri);
	nodePersonaje = scnMgr->getRootSceneNode()->createChildSceneNode(nombreNode);
	//que produzca sombras
	entPersonaje->setCastShadows(true);
	entPersonaje->setVisible(true);

	//lista de ejes
	entPersonaje->getMesh()->buildEdgeList();
	nodePersonaje->attachObject(entPersonaje);

	if(entPersonaje->getSkeleton()!=NULL)
		esqueleto = entPersonaje->getSkeleton();
	//Reseteamos el esqueleto por si acaso
	if(esqueleto!=NULL){
		Skeleton::BoneIterator boneIterator = esqueleto->getBoneIterator();
		int numBones = esqueleto->getNumBones();
			for (int j = 0; j < numBones; j++){
				boneIterator.peekNext()->setManuallyControlled(true);
				boneIterator.peekNext()->resetToInitialState();
				boneIterator.moveNext();
			};
	}

}
//-------------------------------------------------------------------------------------
Personaje::~Personaje(void)
{
}

void Personaje::setPosition(Vector3 p)
{
	nodePersonaje->setPosition(p);
}

Vector3 Personaje::getPosition(void)
{
	return nodePersonaje->getPosition();
}

Vector3 Personaje::getAbsolutePosition(void)
{
	return nodePersonaje->_getDerivedPosition();
}

AnimationState* Personaje::getAnimation(String name){
	return entPersonaje->getAnimationState(name);
}

//Obtiene un hueso directamente del esqueleto
Bone* Personaje::getBone(String name){
	return esqueleto->getBone(name);
}

//Devuelve la posici�n absoluta del hueso, calcul�ndola a partir de la posici�n, orientaci�n y escala del modelo al que se la asocia (el nodo).
Vector3	Personaje::getBoneAbsolutePosition(String name){
	Bone* b = getBone(name);
	return nodePersonaje->_getDerivedPosition() + nodePersonaje->_getDerivedOrientation() * nodePersonaje->_getDerivedScale() * b->_getDerivedPosition();
}

SceneNode* Personaje::getNode(){
	return nodePersonaje;
}

void  Personaje::addIKElement(IKElement *element){
	//al a�adir el elemento le metemos directamente el nodo del personaje
	element->setNode(nodePersonaje);
	elementosIK.insert(pair<string, IKElement*>(string(element->getName().c_str()),element));
}

IKElement* Personaje::getIKElement(String name){
	return elementosIK[string(name.c_str())];
}

void Personaje::addBoneToIKElement(String elementName, String boneName, DOF_constraint dof){
	if(elementosIK[string(elementName.c_str())] != NULL)
		elementosIK[string(elementName.c_str())]->addBone(getBone(boneName), dof);
}

double Personaje::VectorSquaredDistance(Vector3 *v1, Vector3 *v2)
{
	double retValue = (	((v1->x - v2->x) * (v1->x - v2->x)) + 
		((v1->y - v2->y) * (v1->y - v2->y)) + 	
		((v1->z - v2->z) * (v1->z - v2->z)) );
	return retValue; 	
}

///////////////////////////////////////////////////////////////////////////////
// Funci�n:		ComputeCCDLink3D
// Prop�sito:	Calcula una Soluci�n IK a una posici�n objeto del efector en 3D
// Argumentos:	Punto meta (x,y,z), elemento a mover, numero de intentos
// Devuelve:	TRUE si existe soluci�n, FALSE si la posici�n cae fuera de alcance
///////////////////////////////////////////////////////////////////////////////		
bool Personaje::ComputeCCDLink3D(Vector3 endPos, String IKElementName, int numTries, bool damping){
	/// Local Variables ///////////////////////////////////////////////////////////
	Vector3		rootPos,curEnd,targetVector,curVector,crossResult;
	IKElement*	m_Link = elementosIK[string(IKElementName.c_str())];


	//double		cosAngle,turnAngle;
	Degree		turnDeg;
	int			link,tries;
	Quaternion  aQuat;
	int			EFFECTOR_POS = m_Link->posEffector;
	Bone**		m_bones;
	
	//////////////////////////////////////////////////////////////////////////////////
	//	// Empezamos por el ultimo hueso de la lista, sin contar el efector (no angulamos
	//el efector)	m_bones = m_Link->bones;
	link = EFFECTOR_POS - 1;
	tries = 0;						// LOOP COUNTER SO I KNOW WHEN TO QUIT
	m_bones = m_Link->bones;

	//lineList.clear();
	do
	{
		// THE COORDS OF THE X,Y,Z POSITION OF THE ROOT OF THIS BONE IS IN THE MATRIX
		// TRANSLATION PART WHICH IS IN THE 12,13,14 POSITION OF THE MATRIX
		// Calculamos la posici�n real de este hueso
		rootPos = getBoneAbsolutePosition(m_bones[link]->getName());
		// y la del efector
		curEnd = getBoneAbsolutePosition(m_bones[EFFECTOR_POS]->getName());

		// Comprobamos si ya estamos cerca
		if (VectorSquaredDistance(&curEnd, &endPos) > IK_POS_THRESH)
		{
			// Vector a la posici�n actual del efector
			curVector = curEnd - rootPos;
			//debugLine(curEnd,rootPos);
			// Vector a la posici�n deseada del efector
			targetVector = endPos - rootPos;
			//debugLine(endPos,rootPos);

			curVector.normalise();
			targetVector.normalise();

			// El producto escalar nos da el coseno del angulo deseado
			//cosAngle = targetVector.dotProduct(curVector);
			crossResult = curVector.crossProduct(targetVector);
			crossResult.normalise();

			Radian turnDegRad = targetVector.angleBetween(curVector);

			//// DAMPING
			if(damping){
				if (turnDegRad > Radian(0.01f)){
					turnDegRad = Radian(0.01f); 
				}
			}


			//AxisAngleToQuat(&crossResult,turnAngle,&aQuat);


			aQuat = Quaternion(turnDegRad,crossResult);

			//aQuat = curVector.getRotationTo(targetVector,Vector3::NEGATIVE_UNIT_Z);
			//Radian pitch = aQuat.getPitch();
			//Radian yaw  = aQuat.getYaw();
			//Radian roll = aQuat.getRoll();

			m_bones[link]->rotate(aQuat, Ogre::Node::TransformSpace::TS_WORLD);


			// HANDLE THE DOF RESTRICTIONS IF I WANT THEM
			//if (m_DOF_Restrict)

			CheckDOFRestrictions(m_bones[link],m_Link->dofs[link]);

		/*	if(!m_bones[link]->getName().compare("HombroDer") && angManosDebug){
				printAngles(m_bones[link]);
			}*/
			//m_bones[link]->setOrientation(qat);
			m_bones[link]->_update(true,false);

			if (--link < 0) 
				link = EFFECTOR_POS - 1;	// START OF THE CHAIN, RESTART
		}
		// Salimos si estamos suficientemente cerca o hemos estado rulandolo durante el tiempo suficiente
	} while (tries++ < numTries && 
		VectorSquaredDistance(&curEnd, &endPos) > IK_POS_THRESH);

	if (tries == numTries)
		return FALSE;
	else
		return TRUE;
}

bool Personaje::ComputeCCDLink3D(Vector3 endPos, String link, int numTries){
	return ComputeCCDLink3D(endPos,link,numTries,false);
}

///////////////////////////////////////////////////////////////////////////////////
////// Procedure:	CheckDOFRestrictions
////// Purpose:	Se asegura que el hueso est� dentro de sus l�mites DOF
////// Arguments:	Link
///////////////////////////////////////////////////////////////////////////////////		
void Personaje::CheckDOFRestrictions(Bone *link, DOF_constraint dof)
{
	/// Local Variables ///////////////////////////////////////////////////////////
	Quaternion quat;		// PLACE TO STORE EULER ANGLES
	Matrix3 mat;
	Radian x, y, z;
	Degree dX,dY,dZ;
	///////////////////////////////////////////////////////////////////////////////


	// FIRST STEP IS TO CONVERT LINK QUATERNION BACK TO EULER ANGLES
	//quaternion-> matrix -> matrix::toEulerAngle->euler

	link->getOrientation().ToRotationMatrix(mat);
	mat.ToEulerAnglesXYZ(x,y,z);

	dX= Degree(x);
	dY= Degree(y);
	dZ= Degree(z);


	// CHECK THE DOF SETTINGS
	if (dX.valueDegrees() >dof.max_x) 
		dX = Degree(dof.max_x);
	if (dX.valueDegrees() <dof.min_x) 
		dX = Degree(dof.min_x);

	if (dY.valueDegrees() >dof.max_y) 
		dY = Degree(dof.max_y);
	if (dY.valueDegrees() <dof.min_y) 
		dY = Degree(dof.min_y);

	if (dZ.valueDegrees() >dof.max_z) 
		dZ = Degree(dof.max_z);
	if (dZ.valueDegrees() <dof.min_z) 
		dZ = Degree(dof.min_z);

	// BACK TO QUATERNION
	//euler->matrix::fromEulerAngle->matrix->quaternion
	mat.FromEulerAnglesXYZ(Radian(dX),Radian(dY),Radian(dZ));
	quat.FromRotationMatrix(mat);
	link->setOrientation(quat);

}

void Personaje::extractAnglesFromBone(Bone *link, Vector3 *angles){
	Quaternion quat = link->getOrientation();
	Matrix3 mat;
	quat.ToRotationMatrix(mat);
	Radian x, y, z;
	Degree dX,dY,dZ;
	mat.ToEulerAnglesXYZ(x,y,z);

	dX= Degree(x);
	dY= Degree(y);
	dZ= Degree(z);

	angles->x = x.valueDegrees();
	angles->y = y.valueDegrees();
	angles->z = z.valueDegrees();
}
//Operaci�n contraria, asigna los �ngulos a un hueso
void Personaje::setAngles(Bone* link, Vector3 *angles){
	/// Local Variables ///////////////////////////////////////////////////////////
	Quaternion quat;		// PLACE TO STORE EULER ANGLES
	Matrix3 mat;
	Radian x, y, z;
	Degree dX,dY,dZ;
	///////////////////////////////////////////////////////////////////////////////
	
	// FIRST STEP IS TO CONVERT LINK QUATERNION BACK TO EULER ANGLES
	//quaternion-> matrix -> matrix::toEulerAngle->euler
	
	dX= Degree(angles->x);
	dY= Degree(angles->y);
	dZ= Degree(angles->z);
	
	// BACK TO QUATERNION
	//euler->matrix::fromEulerAngle->matrix->quaternion
	mat.FromEulerAnglesXYZ(Radian(dX),Radian(dY),Radian(dZ));
	quat.FromRotationMatrix(mat);
	link->setOrientation(quat);
}


