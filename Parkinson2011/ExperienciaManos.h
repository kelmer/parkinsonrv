/*
-----------------------------------------------------------------------------
Filename:    ExperienciaManos.h
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
*/
#ifndef __ExperienciaManos_h_
#define __ExperienciaManos_h_

#include "StdAfx.h"
#include "Experiencia.h"
/**
* Define el comportamiento de las experiencias de control con manos (Experiencia2 y Experiencia1).
*
* Este tipo de experiencia sit�a al paciente con sus dos manos sobre la mesa. El entorno virtual reproduce 
* este escenario, en una habitaci�n as�ptica en la que el paciente se ve a s� mismo sentado frente a una mesa.
*
* Comunes a estas experiencias se encuentra la presencia de cinem�tica inversa y sus puntos efectores adecuados, as� como cadenas de huesos involucradas.
* El escenario y el personaje son asimismo comunes a ambos.
*
* @todo Resto de documentaci�n de esta clase
*
* @author Gabriel Sanmart�n D�az
*/

class ExperienciaManos : public Experiencia
{
public:
    ExperienciaManos(int protocolos, int dataPerProtocol, int expData);
    virtual ~ExperienciaManos(void);
	virtual void	createScene(void);

protected:
	void			createScenario(void);
	void			createPersonaje(void);
	void			createLights(void);
	void			initAnimations(void);
	void			createIKElements(void);

	//Animaciones
	AnimationState* as_dedeoDer;
	AnimationState* as_dedeoEsq;
	AnimationState* as_dianaRectoDta;
	AnimationState* as_dianaCruceDta;
	AnimationState* as_dianaRectoEsq;
	AnimationState* as_dianaCruceEsq;

	//Debug
	bool			showIKPoints;				//muestra los puntos de destino de los elementos IK
	bool			posManoDebug;				//muestra los valores posicionales de las manos

	/**
	*
	* Determinan las posiciones iniciales de las manos para salvaguardarlas a la hora de recalibrar
	*
	*/
	Vector3			posInicialManoDer; 
	Vector3			posInicialManoEsq;


	/** @name Variables de IK
	* Vectores que especifican la posici�n deseada para el c�lculo de IK
	*/
	//@{
	Vector3			posIKManoDerecha;		///< Posicion de la mano derecha
	Vector3			posIKManoIzquierda;		///< Posicion mano izquierda
	Vector3			posIKDedoDerecho;
	Vector3			posIKDedoIzquierdo;
	Vector3			posNudilloDerecho; 
	Vector3			posNudilloIzquierdo;
	//@}

	/** @name Variables para el debug visual de IK
	* Muestran esferas en los puntos de IK para debug
	*/
	//@{
	Entity*			puntomanoDer;				//entidad del punto de IK mano derecha
	SceneNode*		puntoManoDerNode;
	Entity*			puntomanoIzq;				//entidad del punto de IK mano izda
	SceneNode*		puntoManoIzqNode;
	Entity*			puntodedoDer;				//entidad del punto de IK dedo derecho
	SceneNode*		puntoDedoDerNode;
	Entity*			puntodedoIzq;				//entidad del punto de IK dedo izdo
	SceneNode*		puntoDedoIzqNode;
	//@}

	//Cubo para ver los limites marcados por el movimiento dle dedo
	ManualObject*	IKlimitCubeDer;
	SceneNode*		IKCubeDerNode;

};

#endif // #ifndef __ExperienciaManos_h_