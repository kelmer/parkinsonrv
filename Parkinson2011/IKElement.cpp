/*
-----------------------------------------------------------------------------
Filename:    IKElement.cpp
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
*/
#include "StdAfx.h"
#include "IKElement.h"

IKElement::IKElement(String name, SceneNode *sNode): node(sNode), numBones(0), posEffector(0), firstBone(0), nombre(name){

}

IKElement::IKElement(String name): node(0), numBones(0), posEffector(0), firstBone(0), nombre(name){

}

IKElement::~IKElement(){

}

void IKElement::addBone(Bone* bone, DOF_constraint dof){
	if(numBones<6){
		bones[numBones] = bone;
		dofs[numBones] = dof;
		numBones++;
	}
}

Bone* IKElement::getBone(int idx){
	return bones[idx];
}

//Permite cambiar el hueso effector
void IKElement::setEffector(int n){
	posEffector = n;
}


//Permite cambiar el primer hueso de la cadena
void IKElement::setFirstBone(int n){
	firstBone = n;
}


void IKElement::setNode(SceneNode *sNode){
	node = sNode;
}

String IKElement::getName(){
	return nombre;
}