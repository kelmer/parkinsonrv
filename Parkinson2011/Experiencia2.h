/*
-----------------------------------------------------------------------------
Filename:    Experiencia2.h
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
*/
#ifndef __Experiencia2_h_
#define __Experiencia2_h_

#include "StdAfx.h"
#include "ExperienciaManos.h"


// Dianas
#define DIANA_DERECHA     9  // son estos valores porque estos son los identificadores de los objetos diana dentro del modelo de la habitaci�n (ahora tiene 11 objetos, los 2 �ltimos -9 y 10- son las dianas)
#define DIANA_IZQUIERDA  10
#define BOTH_DIANAS      20
#define NINGUNA_DIANA    -1

#define DIANA_ENCENDIDA  1 
#define DIANA_APAGADA    0

#define ANG_CABEZA_01  0
#define ANG_CABEZA_02  1 
#define ANG_CABEZA_03  2

#define ANG_DEDO_011   3
#define ANG_DEDO_012   4
#define ANG_DEDO_013   5 

#define ANG_DEDO_021   7 
#define ANG_DEDO_022   8
#define ANG_DEDO_023   9

#define POS_MANO_01X  11
#define POS_MANO_01Y   6
#define POS_MANO_01Z  12

#define POS_MANO_02X  13
#define POS_MANO_02Y  10 
#define POS_MANO_02Z  14

#define ANG_MANO_01   15
#define ANG_MANO_02   16


enum AnimOptions {
	DerechoRecto = 0,
    DerechoOblicuo = 1,
    IzquierdoRecto = 2,
    IzquierdoOblicuo = 3
};

enum IllumOptions {
	Random = 0,
	LeftPanel = 1,
	RightPanel = 2
};


/**
* Estructura que almacena la comparaci�n de cada punto detectado por las c�maras con los puntos identificados en el frame anterior.
*
* Almacena las referencias al punto v�lido del frame anterior y al punto detectado, la distancia entre ambos (distancia 2D + diferencia de ratio), y los �ndices los mismos dentro de sus respectivos arrays. 
* Esta estructura permite registrar siempre las distancias entre todos los puntos detectados y los puntos v�lidos para simplificar el algoritmo decisor de correspondencia temporal empleado en Experiencia2.
*
* @author Gabriel Sanmart�n D�az
* @see Experiencia2
*/
typedef struct parIndicePunto {
	Vector3 puntoFinal;
	int indicePFinal;
	Vector3 puntoDetectado;
	int indicePDetectado;
	float distancia;
} parIndicePunto;

/**
* Estructura los par�metros de control de cada uno de los protocolos definidos para la Experiencia1.
*
* Compartimentaliza los datos (recibidos a trav�s de la aplicaci�n externa de control) de cada uno de los protocolos de ejecuci�n secuencial en la Experiencia1.
*
* @author Gabriel Sanmart�n D�az
* @see Experiencia1
*/
typedef struct ProtocoloExp2
{
public: 
	int		IndiceProtocolo; //Numero de protocolo
	bool	Invertir; //Invertir lados
	bool	IsActive; //Activa o no activa
	bool    EnNegro; //Saldr� todo en engro o no
	bool	AutoAnim; //Animaci�n o tracking
	int		ActiveAnim; //Qu� animacion est� activa
	int		ActiveIllum; //Qu� diana est� activa 
	
	float	TimeToShowTarget; //Tiempo para que aparezca la diana
	float	TimeToStartAnimation; //tiempo para que inicie animacion
	float	TimeToEndAnimation; //tiempo de fin de animacion (velocidad = tiempo fin - tiemo inicio)
	float	ExtraWaitingTime; //tiempo extra antes de finalizar protocolo
	 
	int		CyclesNextProtocol; //Ciclos para el siguiente protocolo
	float	TimeNextProtocol; //Tiempo para el siguiente protocolo
	bool	PrioridadCiclos; //Ciclos o tiempo?
	int		CiclosEntrePulso;

	float DistanceBwnTargets; //Distancia entre dianas
	bool  ShouldLightenOnTouch; //deberiamos hacer caso al tiempo de iluminacion de diana
	float TimeToLightenTarget; //tiempo para ILUMINAR diana.
	float TimeToStopLighten; //tiempo para DEJAR DE ILUMINAR diana
}ProtocoloExp2;

/**
* Define el comportamiento de la Experiencia2 (dianas + arm-reaching).
*
* Esta experiencia sit�a al paciente con sus dos manos sobre la mesa. El entorno virtual reproduce 
* este escenario, en una habitaci�n as�ptica en la que el paciente se ve a s� mismo sentado frente a una mesa.
*
* Dos dianas se muestran flotando en el aire sobre la mesa, y se requiere al paciente que las alcance con las manos. 
* El movimiento de alcance se puede realizar mediante tracking de la mano (s�lo se requiere una c�mara cenital), infiriendo
* el movimiento de otros huesos mediante cinem�tica inversa; o bien mediante animaci�n pre-grabada.
*
* Al tocar las dianas se detecta la colisi�n, se ilumina la diana alcanzada y si es la correcta desaparecer�. 
* En ning�n momento aparecer�n ambas dianas al mismo tiempo (solo aparece la diana que el paciente debe alcanzar).
*
* Se registran en archivo diferentes tiempos y datos de evoluci�n de la experiencia.
*
* Se reciben y emiten pulsos desde el m�dulo TAD en diferentes condiciones.
*
* @author Gabriel Sanmart�n D�az
*/

class Experiencia2 : public ExperienciaManos
{
public:
    Experiencia2(int	protocolos, int dataPerProtocol, int expData);
    virtual ~Experiencia2(void);

	typedef enum AnimOptions
    {
		DerechoRecto = 0,
		DerechoOblicuo = 1,
		IzquierdoRecto = 2,
		IzquierdoOblicuo = 3
     };
	 typedef enum IllumOptions
	 {
		Random = 0,
		LeftPanel = 1,
		RightPanel = 2
	 };

protected:
	//ATRIBUTOS
	//DIANAS
	Entity*			entDianaDerecha;
	Entity*			entDianaIzquierda;
	 
	SceneNode*		scnDianaDerecha;
	SceneNode*		scnDianaIzquierda;
	SceneNode*		scnDianasComun;

	Vector3			posDianas;
	Vector3			posDianaDerecha;
	Vector3			posDianaIzquierda;
	
	//Tema colisiones
	//cre� dos esferas, esto esu na cutrada, para las colisiones. Ver si se puede cambiar
	Entity*			manDerSphere;
	Entity*			manIzqSphere;
	SceneNode*		manDerSphereNode;
	SceneNode*		manIzqSphereNode;
	ManualObject*	manualObjectCollision;
	bool			colisiona_diana_izquierda;
	bool			colisiona_diana_derecha;
	bool			colisionWrite;

	//Variables de trabajo de la experiencia (variables circunstanciales para hacerla rular)
	float			velocidadManos;
	AnimationState* as_currentDiana;
	int				mDianaEncendida;
	bool			puedeAparecerDiana;
	bool			colision;
	float			timeToLive;
	
	/**
	* Tiempo transcurrido en la ejecuci�n del ciclo actual
	*/
	float			t_ciclo;


	Vector3			angulos_hombro_der, angulos_codo_der;
	Vector3			angulos_hombro_izq, angulos_codo_izq;
	Vector3			old_angulos_hombro_der, old_angulos_codo_der;
	Vector3			old_angulos_hombro_izq, old_angulos_codo_izq;

	void			setManuallyControlled(bool set);

	/** @name Variables de calibrado
	* La mayor�a proceden del sistema anterior de tracking
	*/
	//@{
	Vector3				puntosDetectados[20];  //Vector con todos los puntos detectados por las c�maras
	Vector3				puntosDetectadosDerecha[5];  //Vector con todos los puntos detectados por las c�maras
	Vector3				puntosDetectadosIzquierda[5];  //Vector con todos los puntos detectados por las c�maras
	int					numPuntos; //numero de puntos detectados
	int					numPuntosDerecha; //numero de puntos detectados
	int					numPuntosIzquierda; //numero de puntos detectados
	Vector3				puntosFinales[4]; //Vector con los puntos candidatos
	Vector3				puntosFinalesDerecha[2]; //Vector con los puntos candidatos
	Vector3				puntosFinalesIzquierda[2]; //Vector con los puntos candidatos
	parIndicePunto		pares[20]; //Conjunto de relaciones puntoFinal - puntoDetectado
	//@}


	float				yCeroDerecho;
	float				yCeroIzquierdo;

	//INHERITED MEMBERS
	void			createCamera(void);	
	void			createScene(void);
	bool			frameRenderingQueued(const FrameEvent& evt);
	void			createScenario(void);
	bool			keyPressed( const OIS::KeyEvent &arg );

	void			initDianas();
	
	//Actualizaci�n
	void			updateVars();
	
	// calcula si la posici�n del dedo est� "tocando" alguna de las dianas
	void			detectaColisiones(ProtocoloExp2 protocol);
	// estado es si est� encendida o apagada
	void			iluminaDiana(int diana, int estado);

	//Divisi�n de la ejecuci�n del frame en funciones
	void			calibrate(void);
	void			executeExperienceFrame(const FrameEvent& evt);
	void			executeProtocol(const FrameEvent& evt);
	void			solveIK(const FrameEvent& evt);
	//a�Ado la posibilidad de usar el m�todo antiguo

	void			solveIKOldMethod(const FrameEvent& evt);
	 
	 //debug
	//atributos y propiedades de la experiencia enviados por la gui
	bool			debugSettings;
	//debug de colisiones (posicion manos y dianas)
	bool			colisionDebug;
	//debug del algoritmo de "detecci�n" de movimiento
	bool			movingDebug;
	//permite mostrar o no el bbox de las dianas
	bool			showDianasBoundindBox;
	//Puntos
	bool			pointsDebug;

	
	 /**
	 * Almacena los 10 protocolos de configuraci�n almacenables.
	 * Se requiri� la posibilidad de disponer (desde la GUI externa) hasta 10 posibles configuraciones de los diferentes 
	 * par�metros que componen la experiencia, poder almacenarlos como archivos y poder ejecutarlos de forma secuencial 
	 * o aleatoria. Todos los par�metros se almacenan en estos "protocolos" de ejecuci�n. 
	 * @see Protocolo
	 */
	 std::vector<ProtocoloExp2>		protocolos; //Los N protocolos configurables

	/**
	 * Imprime al archivo \c file2 los datos de un protocolo.
	 * El requerimiento es que se dumpeen todos los par�metros configurados en la GUI por el investigador y el n�mero de protocolo, para referencia de �ste.
	 * @param p Protocolo a dumpear
	 * @param i N�mero de protocolo (no contenido en la estructura Protocolo)
	 */
	 void			dumpProtocolData(ProtocoloExp2 p, int i);

	 //Comprueba directamente sobre el material si est�n activadas las dianas
	 bool			isDianaDerechaEncendida();
	 bool			isDianaIzquierdaEncendida();
};

#endif // #ifndef __Experiencia2_h_