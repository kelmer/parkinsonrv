/*! 
  \file 
  \brief Definici�n de la clase para la experiencia 1
 */

#ifndef __Experiencia1_h_
#define __Experiencia1_h_

#include "StdAfx.h"
#include "ExperienciaManos.h"
// para playsound
#include <mmsystem.h> 

#define  INPUTPORTEXP1	4020
#define  OUTPUTPORTEXP1	5001

/** @name �ndices posicionales de los valores de tracking
* Indican el �ndice dentro del buffer recibido v�a sockets desde las c�maras, de cada una de las coordenadas de los elementos trackeados.
* @see datosCamaras
*/
//@{
//VALORES Y MANO DERECHA
#define POS_MANDER_Y		4
#define POS_MUNECADER_Y		5
#define POS_DEDODER_Y		6
//VALORES Y MANO IZQUIERDA
#define POS_MANIZQ_Y		8
#define POS_MUNECAIZQ_Y		9
#define POS_DEDOIZQ_Y		10
//VALORES XZ MANO DERECHA
#define POS_MUNECADER_X		11
#define POS_MUNECADER_Z		12
#define	POS_MANDER_X		13
#define POS_MANDER_Z		14
#define POS_DEDODER_X		15
#define POS_DEDODER_Z		16
//VALORES XZ MANO IZQUIERDA
#define POS_MUNECAIZQ_X		17
#define POS_MUNECAIZQ_Z		18
#define	POS_MANIZQ_X		19
#define POS_MANIZQ_Z		20
#define POS_DEDOIZQ_X		21
#define POS_DEDOIZQ_Z		22
//{@

/**
* Estructura los par�metros de control de cada uno de los protocolos definidos para la Experiencia1.
*
* Compartimentaliza los datos (recibidos a trav�s de la aplicaci�n externa de control) de cada uno de los protocolos de ejecuci�n secuencial en la Experiencia1.
*
* @author Gabriel Sanmart�n D�az
* @see Experiencia1
*/
typedef struct Protocolo
{
public: 
	int		IndiceProtocolo;
	bool	Invertir;
	bool	ActivateSound;
	bool	ActivateAnimation;
	bool	SoundSync;
	float	SoundFrequency;
	float	PassiveFrequency;
	float	PostPassiveFrequency;
	float	ActiveFrequency;
	float	AnimationBlending;
	int		CyclesNextProtocol;
	float	TimeNextProtocol;
	bool	IsActive;
	int		CiclosEntrePulso;
	bool	PrioridadCiclos;
	bool    EnNegro;
}Protocolo;

/**
* Define el comportamiento de la llamada Experiencia1 (finger-tapping).
*
* Esta experiencia sit�a al paciente con sus dos manos sobre la mesa. El entorno virtual reproduce 
* este escenario, en una habitaci�n as�ptica en la que el paciente se ve a s� mismo sentado frente a una mesa.
*
* Al paciente se le requieren movimientos de finger-tapping, que pueden ser reproducidos a voluntad del
* investigador a diferentes intervalos y mediante tracking o animaci�n simulada (mediante cinem�tica inversa). 
* Para el caso del tracking se emplear�n tres c�maras, una cenital y dos laterales.
*
* Un sonido permite indicar al paciente cu�ndo ejecutar las �rdenes indicadas por el investigador. Diferentes par�metros pueden 
* configurarse desde la aplicaci�n de control GUI (externa).
*
* Se registran en archivo diferentes tiempos y datos de evoluci�n de la experiencia.
*
* Se reciben y emiten pulsos desde el m�dulo TAD en diferentes condiciones.
*
* @todo Implementaci�n de correspondencia temporal para la Experiencia1, de la misma forma que se hizo en Experiencia2.
* @author Gabriel Sanmart�n D�az
*/
class Experiencia1 : public ExperienciaManos
{
public:
	/**
	* Constructor.
	* Inicializa los archivos, el n�mero de puerto de la GUI (4020), las posiciones iniciales de los puntos de IK, y el orden de protocolos (a un orden secuencial).
	*/
    Experiencia1(int protocolos, int dataPerProtocol, int expData);
	/**
	* Destructor.
	* @todo Actually destruct something.
	*/
    virtual ~Experiencia1(void);

protected:

	 //PROPERTIES
	
	/** @name Variables de la c�mara
	* Variables empleadas para el manejo, posici�n y orientaci�n de la c�mara.
	*/
	//@{

	/** 
	* Posici�n de la c�mara enfocando las manos desde abajo.
	* Por requerimiento del cliente se ha creado una c�mara que enfoca las manos desde una posici�n invertida. Esta c�mara ser� seleccionable mediante las flechas de control arriba y abajo, y se aplicar� mediante un travelling (interpolaci�n entre initialPosCamera y finalPosCamera). 
	*/
	 Vector3			finalPosCamera;			//posicion final enfocando manos desde abajo
	/** 
	* Posici�n actual de la c�mara.
	* Almacena la posici�n en el frame actual de la c�mara, para puntos intermedios de la interpolaci�n y para permitir el movimiento de la misma sin alterar las posiciones inicial y final.
	*/
	 Vector3			currentPosCamera;		//posicion en la que est� actualmente
	/** 
	* Orientaci�n de la c�mara enfocando las manos desde abajo.
	* @see finalPosCamera
	*/
	 Quaternion			finalOrientationCamera; //orientaci�n final
	 /** 
	 * Porcentaje actual de interpolaci�n en el travelling initialPosCamera->finalPosCamera
	 */
	 float				camPosPercentage;		//porcentaje de desplazamiento
	 
	 bool				goingForward;			///< Flag para el movimiento continuo de la c�mara al pulsar una tecla. Se hace necesario este sistema al tratarse de buffered input. (solo se registra el evento de pulsaci�n una vez, que activa/desactiva el flag).
	 bool				goingBackward;			///< Flag para el movimiento continuo de la c�mara
	 
	 //@}

	 //@{
	//! Almacena posici�n y orientaci�n de la c�mara en primer plano sobre cada mano. 
	//! Por requerimiento del cliente se pidi� una c�mara enfocando un primer plano de cada mano. Ativable mediante las flechas derecha e izquierda.
	 Vector3			mCameraManoIzquierda;			//
	 Quaternion			mCameraManoIzquierdaOrientation;//orientacion

	 Vector3			mCameraManoDerecha;				//pos
	 Quaternion			mCameraManoDerechaOrientation;  //orientation
	 //@}


	 /** 
	* Posici�n secundaria de la c�mara.
	* Almacena la posici�n secundaria de la c�mara, que se sit�a frente al avatar y mirando hacia �ste. 
	* @see finalPosCamera
	*/
	 Vector3			frontPosCamera;
	 /** 
	* Orientaci�n secundaria de la c�mara.
	* Almacena la orientaci�n secundaria de la c�mara, que se sit�a frente al avatar y mirando hacia �ste. 
	* @see finalPosCamera
	*/
	 Quaternion			frontOrientationCamera;  //orientation


	 //Par�metros de la experiencia
	 //Variables exclusivas de la experiencia
	 /**
	 * Almacena los 10 protocolos de configuraci�n almacenables.
	 * Se requiri� la posibilidad de disponer (desde la GUI externa) hasta 10 posibles configuraciones de los diferentes 
	 * par�metros que componen la experiencia, poder almacenarlos como archivos y poder ejecutarlos de forma secuencial 
	 * o aleatoria. Todos los par�metros se almacenan en estos "protocolos" de ejecuci�n. 
	 * @see Protocolo
	 */
	 std::vector<Protocolo>		protocolos; //Los N protocolos configurables


	 /**
	 * Contador para la siguiente emisi�n de pulso sonoro.
	 * Mide el tiempo transcurrido entre sonidos para coincidir la emisi�n de estos con lo especificado en el protocolo.
	 **/
	 float				tLastSound;
	 /**
	 * Tiempo activo de la animaci�n
	 * Tiempo de avance de la animaci�n; mide el tiempo entre frames para hacer la avanzar la animaci�n seg�n lo 
	 * transcurrido.
	 **/
	 float				tAnimation;
	 /**
	 * Tiempo pasivo previo a la animaci�n
	 * Se requiri� la posibilidad de especificar por parte del investigador y desde la GUI un tiempo de espera
	 * antes de disparar la animaci�n, llamado tiempo pasivo. La variable mide dicho tiempo desde la finalizaci�n
	 * de la animaci�n anterior y hasta el inicio de la siguiente (siguiente ciclo de aniamci�n).
	 **/
	 float				tAnimacionPasiva;
	 /**
	 * Tiempo pasivo posterior a la animaci�n
	 * Asimismo se requiri� tambi�n la posibilidad de especificar un tiempo de espera posterior a la animaci�n,
	 * llamado tiempo pasivo posterior.
	 **/
	 float				tAnimacionPasivaPost;

	/**
	* Especifica qu� animaci�n de finger-tapping est� activa actualmente. 
	* Cuando se produce el cambio, el puntero a AnimationState se redirige a la animaci�n que se solicite, de esta forma s�lo hay 
	* un c�digo de actualizaci�n de la animaci�n.
	*/
	 AnimationState *	currentDedeo;
	 /**
	 * Mutex para invetir el orden de ejecuci�n de sonido y animaci�n.
	 * Esto requiere una larga explicaci�n. Cuando se hizo la primera revisi�n de la experiencia, se quejaron de que sonido y animaci�n
	 * iban descompensados. Ante las continuas peticiones de "precisi�n absoluta" en la reproducci�n del sonido y la animaci�n (en el contador
	 * de tiempo de ambos, vamos), lo que hice fue crear un mutex que alterna el orden de ejecuci�n de los mismos, es decir, en un frame
	 * se ejecuta primero el sonido y en el siguiente la animaci�n. De esta manera se intenta corregir posibles descompensaciones provocadas
	 * por la nimia diferencia entre la ejecuci�n de un bloque de c�digo y el otro.
	 * Probablemente la diferencia sea inapreciable y esto no fuese necesario, pero placebo o no, col� como preciso (porque en efecto es
	 * todo lo preciso que es posible).
	 */
	 bool				mutexSonido;



	//Huesos a controlar en esta escena.
	Bone* dedo2Der01;
	Bone* dedo2Der02;
	Bone* dedo2Der03;
	Bone* dedo2Esq01;
	Bone* dedo2Esq02;
	Bone* dedo2Esq03;
	Bone* manDer;
	Bone* manEsq;


	 //Variables de debug
	 bool				timeSyncDebug;				///< Activa/desactiva el muestreo en pantalla de los distintos temporizadores


	 /** @name Variables de la fase de calibrado de la experiencia
	* Variables generadas durante la fase inicial de calibrado.
	*/
	//@{
	 float				rollCeroDedoDerecho;		///< Durante el calibrado se mide el roll inicial entre la mano y el dedo, para compensar la inclinaci�n natural de los patrones al situarse uno sobre la mu�eca y otro en el �ndice (quedan inclinados hacia abajo).
	 float				yawCeroDedoDerecho;			///< Dada la posici�n de los patrones, uno en el dedo y otro en el centro de la mano (mu�eca), existe un �ngulo inicial entre ambos, que se recoge en la fase de calibrado en esta variable para aplicar correcci�n durante el tracking.
	 float				yCeroDerecho;				///< Como las c�maras laterales pueden estar a una altura arbitraria, tomo la altura durante la calibraci�n (en la que se exige las manos sobre la mesa) como valor 0 y escalo a partir de ah�.
	 float				rollCeroDedoIzquierdo;		///< Durante el calibrado se mide el roll inicial entre la mano y el dedo, para compensar la inclinaci�n natural de los patrones al situarse uno sobre la mu�eca y otro en el �ndice (quedan inclinados hacia abajo).
	 float				yawCeroDedoIzquierdo;		///< Dada la posici�n de los patrones, uno en el dedo y otro en el centro de la mano (mu�eca), existe un �ngulo inicial entre ambos, que se recoge en la fase de calibrado en esta variable para aplicar correcci�n durante el tracking.
	 float				yCeroIzquierdo;				///< Como las c�maras laterales pueden estar a una altura arbitraria, tomo la altura durante la calibraci�n (en la que se exige las manos sobre la mesa) como valor 0 y escalo a partir de ah�.
	 //@}

	//-----------------------------------METODOS
	 void				createCamera(void);
     void				createScene(void);
	 bool				frameRenderingQueued(const FrameEvent& evt);
	 bool				keyPressed( const OIS::KeyEvent &arg );
	 bool				keyReleased( const OIS::KeyEvent &arg );

	 //Actualizaci�n
	 void				updateVars(void);
	 /**
	 * Imprime al archivo \c file1 los datos de un protocolo.
	 * El requerimiento es que se dumpeen todos los par�metros configurados en la GUI por el investigador y el n�mero de protocolo, para referencia de �ste.
	 * @param p Protocolo a dumpear
	 * @param i N�mero de protocolo (no contenido en la estructura Protocolo)
	 */
	 void				dumpProtocolData(Protocolo p, int i);

	 //Divisi�n de la ejecuci�n del frame en funciones
	 void				calibrate(void);
	 void				executeExperienceFrame(const FrameEvent& evt);
	 void				executeProtocol(const FrameEvent& evt);
	 void				solveIK(const FrameEvent& evt);
};

#endif // #ifndef __Experiencia1_h_