/*
-----------------------------------------------------------------------------
Filename:    Experiencia.cpp
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
*/
#include "StdAfx.h"
#include "Experiencia.h"

//-------------------------------------------------------------------------------------
Experiencia::Experiencia(int protocolos, int dataPerProtocol, int expData): fileEventoA("eventoA"), 
								fileEventoB("eventoB"), 
								mMove(50),
								nProtocolos(protocolos),
								calibrationDone(false),
								nDataPerProtocol(dataPerProtocol),
								nExpData(expData),
								nDatosGUI(150),
								outputPort(5001), //en principio el puerto de salida es com�n
								inputPortCam(3305),
								mDebugPanel(0),
								indice_protocolo_actual(0),
								indice_protocolo_random(0),
								numCiclosEsperados(0),
								numCiclosTranscurridos(0),
								randomized(false),
								reiniciarProtocolos(false),
								random(false),
								resetExp(false),
								secs(0),
								t_protocolo(0),
								tiempoTotal(0),
								tiempoInicial(0),
								tiempoActual(0),
								blackSwitch(false),
								multX(40),
								multY(60),
								started(false),
								recepcionOK(false)
{

	string servAddress = "www.google.es";

	
	//sock = TCPSocket(servAddress, 80);
	//Vamos a inicializar siempre los sockets
	receiverCam = new sockets::J_Recibir();
	receiverGUI = new sockets::J_Recibir();
	sender		= new sockets::J_Enviar();

	nDatosGUI = nExpData + nDataPerProtocol * nProtocolos;
	datosGUI = new float[nDatosGUI];
	for(int i=0;i<nDatosGUI;i++)
		datosGUI[i] = 0;
	timer = CHiResTimer();
}
//-------------------------------------------------------------------------------------
Experiencia::~Experiencia(void)
{
	//Por si acaso
	//pulseController.close();
	//file2.close();
	//fileEventoA.close();
	//fileEventoB.close();
}
//-------------------------------------------------------------------------------------
bool Experiencia::frameRenderingQueued(const FrameEvent& evt)
{
	//Actualizamos valores de debug
	mDebugPanel->setAllParamNames(mDebugLineNames);
	mDebugPanel->setAllParamValues(mDebugLineValues);

	bool retValue = BaseApplication::frameRenderingQueued(evt);
	secs = timer.GetElapsedSeconds();
	tiempoTotal += secs;			
	return retValue;
}
//-------------------------------------------------------------------------------------
void Experiencia::initCommonParameters(void)
{
	Ogre::MaterialManager::getSingleton().setDefaultTextureFiltering(TFO_ANISOTROPIC);
    Ogre::MaterialManager::getSingleton().setDefaultAnisotropy(8);

	mWindow->setActive(true); //Hace que la ventana est� activa
	mWindow->setAutoUpdated(true); //Que se actualice siempre
	mWindow->setDeactivateOnFocusChange(false); //Y que siga haci�ndolo aun cuando no sea la ventana activa

	//Luz ambiente
	mSceneMgr->setAmbientLight(ColourValue(0.8f, 0.8f, 0.8f)); //Luz ambiente

	// set shadow properties

	/*mSceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);
	   mSceneMgr->setShadowTextureSettings(4096,1);
	   mSceneMgr->setShadowFarDistance(20);
	   mSceneMgr->setShadowColour(Ogre::ColourValue(0.73f,0.73f,0.73f,0.5f));*/

	//mSceneMgr->setShadowTextureSelfShadow(true);	
}

void Experiencia::setBlankScreen(bool set){
	nodeNegro->setVisible(set);
	if(set){
			nodeNegro->setPosition(mCamera->getDerivedPosition() + mCamera->getDirection()*2.5);
			nodeNegro->setOrientation(mCamera->getDerivedOrientation());
			//Desactivamos el movimiento de camara tanto de teclado como de raton
			mCameraMan->setTopSpeed(0); 
			mCameraMan->setMouseMultX(0);
			mCameraMan->setMouseMultY(0);
	}
	else {
			//Restablecemos velocidades de movimiento
			mCameraMan->setTopSpeed(mMove);
			mCameraMan->setMouseMultX(multX);
			mCameraMan->setMouseMultY(multY);
	}

}
//TECLAS COMUNES
bool Experiencia::keyPressed( const OIS::KeyEvent &arg )
{
		/**
	*  Lista de teclas comunes:
	*		-# V apaga/enciende "la luz" (pantalla negra)
	*		-# C para alternar la velocidad de desplazamiento (traslado) de la c�mara
	*		-# B para activar/desactivar el multiplicador de sensibilidad del rat�n
	*		-# � para activar/desactivar el panel de debug
	*/
	BaseApplication::keyPressed(arg);
    if (arg.key == OIS::KC_V)   // toggle visibility of advanced frame stats
    {
		blackSwitch = !blackSwitch;
		setBlankScreen(blackSwitch);
	
    }
	else if(arg.key == OIS::KC_C)
	{
		switch(mMove)
		{
			case 50:
				mMove=1;
				break;
			case 20:
				mMove=50;
				break;
			case 10:
				mMove=20;
				break;
			case 5:
				mMove=10;
				break;
			case 1:
				mMove=5;
				break;
		}		
		mCameraMan->setTopSpeed(mMove);
	}
	else if(arg.key == OIS::KC_B)
	{
		mCameraMan->setApplyMultiplier(!mCameraMan->getApplyMultiplier());
	}

	
	//RESET DE CAMARA
	else if(arg.key == OIS::KC_F1 || arg.key == OIS::KC_1)
	{
		mCamera->setPosition(initialPosCamera);
		mCamera->setOrientation(initialOrientation);
	}
	//ACTIVAR/DESACTIVAR PANEL DE DEBUG
	else if (arg.key == OIS::KC_GRAVE)   // toggle visibility of even rarer debugging details
    {
        if (mDebugPanel->getTrayLocation() == OgreBites::TL_NONE)
        {
            mTrayMgr->moveWidgetToTray(mDebugPanel, OgreBites::TL_TOPLEFT, 0);
            mDebugPanel->show();
        }
        else
        {
            mTrayMgr->removeWidgetFromTray(mDebugPanel);
            mDebugPanel->hide();
        }
    }
	
    return true;
}

bool Experiencia::keyReleased( const OIS::KeyEvent &arg )
{
	return BaseApplication::keyReleased(arg);
}
//-------------------------------------------------------------------------------------
void Experiencia::createCamera(void)
{
	BaseApplication::createCamera();
	// Ponemos la c�mara en 0,0,0 ya que manejaremos la posicion mediante nodes
    mCamera->setPosition(Ogre::Vector3(0,0,0));
	mCamera->setNearClipDistance(0.7f);

	//Inicializamos la c�mara primaria
	nodeCamera = mSceneMgr->createSceneNode("CameraNode");
	nodeCamera->setPosition(0,0,0);
	nodeCamera->attachObject(mCamera);

	mCameraMan->setTopSpeed(mMove);

	Ogre::Plane plane = Plane(Vector3::UNIT_Z,1); //Your generated Plane, whichever you will need
	Ogre::MeshManager::getSingleton().createPlane("ground", "Mios", 
														plane, 
														15, 
														15, 
														1, 
														1, 
														true, 
														1, 
														5, 
														5);
	entNegro = mSceneMgr->createEntity( "GroundEntity", "ground" );
	nodeNegro = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	nodeNegro->attachObject(entNegro);
	entNegro->setVisible(true);
	nodeNegro->setVisible(false);
}//-------------------------------------------------------------------------------------

void Experiencia::destroyScene(void)
{
	//CERRAMOS EL CONTROLLER!!
	pulseController.close();
	//file1.close();
	//file2.close();
	//fileEventoA.close();
	//fileEventoB.close();
}

void Experiencia::createScene(void)
{
	pulseController.init();
	initSockets();
	// create your scene here :)
	initCommonParameters();		
	createScenario();
	createPersonaje();
	createLights();
	initAnimations();
	createDebugPanel();

	//ahora inicializamos el cronometro
	timer.Init();
	//medimos el tiempo de inicio
	tiempoInicial = clock() * CLK_TCK;

	//material para ennegrecer la escena
	entNegro->setMaterialName( "black" );

	//multiplicadores para la camara
	mCameraMan->setMouseMultX(multX);
	mCameraMan->setMouseMultY(multY);
	mCameraMan->setApplyMultiplier(false);

	//desactivamos el panel de fps
	mTrayMgr->hideFrameStats();
}

void Experiencia::initSockets(void){
	//B�sicamente, creamos los sockets asoci�ndolos a sus puertos
	receiverCam->setPort(inputPortCam);
	receiverGUI->setPort(inputPortGUI);
	sender->setPort(outputPort);

}

void Experiencia::createDebugPanel(void)
{
	mDebugPanel = mTrayMgr->createParamsPanel(OgreBites::TL_NONE, "DebugPanel", 400, 20);
    mDebugPanel->hide();
	mTrayMgr->hideLogo();

}

void Experiencia::updateSockets(void){
	

	//PRIMERO LA RECEPCION DE LOS VALORES DE LAS CAMARAS
	//inicializamos el buffer
	 
	receiverCam->setBuffer((void *)datosCamaras, sizeof(datosCamaras));
	if(receiverCam->sync() >=0){
		recepcionOK = true;
	}
	else
	{
		recepcionOK = false;
	}
	//receiverGUI = new sockets::J_Recibir();
	//receiverGUI->setPort(inputPortGUI);
	mDebugLineNames.push_back("Recibiendo CAM:");
	mDebugLineValues.push_back(StringConverter::toString(recepcionOK));	
	//AHORA LA RECEPCI�N PARA EL GUI
	receiverGUI->setBuffer((void *)datosGUI, sizeof(float)*nDatosGUI);
	if (receiverGUI->sync() >= 0)
	{
		//TODO
		recepcionOK = true;
	}
	else {
		recepcionOK = false;
	}
	mDebugLineNames.push_back("Recibiendo GUI:");
	mDebugLineValues.push_back(StringConverter::toString(recepcionOK));
}

int Experiencia::getEllapsedMillisecs(void){
	//obtenemos los milisegundos transcurridos
	int ms = (int)(tiempoTotal);
	float fs = tiempoTotal - (float)(ms);
	tiempoActual = clock() * CLK_TCK;
	
	int elapTicks;
	double elapMilli;
	elapTicks = tiempoActual - tiempoInicial;
	elapMilli = elapTicks/1000;

	return elapMilli;
}

void Experiencia::clearDebug(void){
	mDebugLineNames.clear();
	mDebugLineValues.clear();
	
	mDebugLineNames.push_back("TiempoTotal");
	mDebugLineValues.push_back(StringConverter::toString(tiempoTotal));

}

void Experiencia::printLine(String title, String value){
	mDebugLineNames.push_back(title);
	mDebugLineValues.push_back(value);
}
void Experiencia::printLine(String title, float value){
	printLine(title, StringConverter::toString(value));
}
void Experiencia::printLine(String title, int value){
	printLine(title, StringConverter::toString(value));
}
void Experiencia::printLine(String title, Vector3 value){
	printLine(title, StringConverter::toString(value));
}
void Experiencia::printLine(String title, bool value){
	printLine(title, StringConverter::toString(value));
}
void Experiencia::printLine(String title, char *value){

	String cosa = String(value);
	printLine(title, cosa);
}
void Experiencia::printLine(String title, Degree value){
	printLine(title, StringConverter::toString(value));
}
