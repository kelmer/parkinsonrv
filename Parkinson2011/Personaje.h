/*
-----------------------------------------------------------------------------
Filename:    Personaje.h
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
*/
#ifndef __Personaje_h_
#define __Personaje_h_

#include "StdAfx.h"
#include "IKElement.h"

#define MAX_IK_TRIES		100		// VECES QUE ITERA EL BUCLE CCD (INTENTOS = # / ENLACES) 
#define IK_POS_THRESH		0.001f	// UMBRAL PARA DECIR QUE HAY �XITO EN EL ALGORITMO CCD


using namespace Ogre;
using namespace std; 


/**
* Clase definitoria de un Personaje dentro de cada Experiencia.
*
* Facilita la labor de trabajo sobre el personaje gestionando su introducci�n en la escena y sus transformaciones, 
* la obtenci�n y gesti�n de animaciones, el control individual de huesos, y las soluciones IK.
*
* Todas las experiencias contienen un objeto de esta clase.
*
* @author Gabriel Sanmart�n D�az
*/
class Personaje
{
	public:
		Personaje(SceneManager* scnMgr, String nombreEntity, String nombreNode, String uri);
		virtual ~Personaje(void);

		//el getPosition de toda la vida
		Vector3					getPosition();
		//el cl�sico getDerived position
		Vector3					getAbsolutePosition();
		//cambia la posici�n del personaje
		void					setPosition(Vector3 p);
		//obtiene el animation state
		AnimationState*			getAnimation(String name);
		//obtiene un hueso
		Bone*					getBone(String name);				
		//obtiene la posici�n absoluta de un hueso
		Vector3					getBoneAbsolutePosition(String name);
		//para a�adir un elemento de IK
		void					addIKElement(IKElement *);
		//Obtener el elemento IK
		IKElement*				getIKElement(String name);
		//devuelve el nodo del personaje 
		SceneNode*				getNode();
		//devuelve la entidad del personaje
		Entity*					getEntity();
		//A�ade un hueso al IKElement
		void					addBoneToIKElement(String elementName, String boneName, DOF_constraint dof);
		
		//Resuelve un problema de cinem�tica inversa
		bool					ComputeCCDLink3D(Vector3 endPos, String IKElementName, int numTries, bool damping);
		bool					ComputeCCDLink3D(Vector3 endPos, String link, int numTries);
		//Comprueba que la soluci�n no infrinja las restricciones impuestas
		void					CheckDOFRestrictions(Bone *link, DOF_constraint dof);
		//Extrae los �ngulos de Euler de un hueso
		void					extractAnglesFromBone(Bone *link, Vector3 *angles);
		//Operaci�n contraria, asigna los �ngulos a un hueso
		void					setAngles(Bone* link, Vector3 *angles);

protected:
		Entity*					entPersonaje;
		SceneNode*				nodePersonaje;
		String					nombreEntidad;
		String					nombreNodo;
		Skeleton*				esqueleto;
		//vector que contiene todos los elementos de IK
		std::map<string, IKElement*>	elementosIK;

		//distancia entre dos vectores elevada al cuadrado
		double					VectorSquaredDistance(Vector3 *v1, Vector3 *v2);
};


#endif // #ifndef __Personaje_h_
