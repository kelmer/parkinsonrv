/*
-----------------------------------------------------------------------------
Filename:    Util.h
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
*/
#ifndef __Util_h_
#define __Util_h_

#define ENDSTR '\0'



#include "StdAfx.h"


#ifndef _CLOCK_T_DEFINED
typedef long clock_t;
#define _CLOCK_T_DEFINED
#endif


class Util
{
public:
	static void getGregorianDate(char *f){
		char *d, *m, *a;
		d = (char *)malloc(sizeof(char) * 3);
		m = (char *)malloc(sizeof(char) * 3);
		a = (char *)malloc(sizeof(char) * 5);    
		getDay(d); getMonth(m); get_anho2(a);   
		f[0]  = d[0]; f[1]  = d[1]; f[2]  = '-';
		f[3]  = m[0]; f[4]  = m[1]; f[5]  = '-';
		f[6]  = a[0]; f[7]  = a[1]; f[8]  = a[2]; f[9]  = a[3]; f[10] = ENDSTR;     
	};

	static char* num2char(int num){
		char *ret;   
		return (itoa(num, ret,10)); 
	}

	static char *getCompleteDate(){
	   time_t lt;
	   char *fecha;
	   lt = time(NULL);
	   return (fecha = ctime(&lt));
	}

	static void getDay(char *num){
		char *fecha_completa = getCompleteDate(); //, num[3];
		num[1] = fecha_completa[9];
		num[2] = ENDSTR;  
	}

	static void getMonth(char *num){
		char *fecha_completa = getCompleteDate(); //, *num;
		//   num = mes2numChar();
		mes2numChar(num);
		//   return num;
	}

	static void mes2numChar(char *num){
		char *fecha_completa = getCompleteDate(); //, num[3];

		if(strstr(fecha_completa, "Jan")){      num[0] = '0'; num[1] = '1'; num[2] = ENDSTR; }
		else if(strstr(fecha_completa, "Feb")){ num[0] = '0'; num[1] = '2'; num[2] = ENDSTR; }
		else if(strstr(fecha_completa, "Mar")){ num[0] = '0'; num[1] = '3'; num[2] = ENDSTR; }
		else if(strstr(fecha_completa, "Apr")){ num[0] = '0'; num[1] = '4'; num[2] = ENDSTR; }
		else if(strstr(fecha_completa, "May")){ num[0] = '0'; num[1] = '5'; num[2] = ENDSTR; }
		else if(strstr(fecha_completa, "Jun")){ num[0] = '0'; num[1] = '6'; num[2] = ENDSTR; }
		else if(strstr(fecha_completa, "Jul")){ num[0] = '0'; num[1] = '7'; num[2] = ENDSTR; }
		else if(strstr(fecha_completa, "Aug")){ num[0] = '0'; num[1] = '8'; num[2] = ENDSTR; }
		else if(strstr(fecha_completa, "Sep")){ num[0] = '0'; num[1] = '9'; num[2] = ENDSTR; }
		else if(strstr(fecha_completa, "Oct")){ num[0] = '1'; num[1] = '0'; num[2] = ENDSTR; }
		else if(strstr(fecha_completa, "Nov")){ num[0] = '1'; num[1] = '1'; num[2] = ENDSTR; }
		else if(strstr(fecha_completa, "Dec")){ num[0] = '1'; num[1] = '2'; num[2] = ENDSTR; }  
		//   return num;      
	}

	static void get_anho2(char *num){
		char *fecha_completa = getCompleteDate(); //, *num;
		num[0] = fecha_completa[20];
		num[1] = fecha_completa[21];
		num[2] = fecha_completa[22];
		num[3] = fecha_completa[23];
		num[4] = ENDSTR;
		//   return num;
	}

	static void getCompleteTime(char *num){
	   char *fecha_completa = getCompleteDate(); //, *num;

	   num[0] = fecha_completa[11];
	   num[1] = fecha_completa[12];
	   num[2] = '.';
	   num[3] = fecha_completa[14];
	   num[4] = fecha_completa[15];
	   num[5] = '.';
	   num[6] = fecha_completa[17];
	   num[7] = fecha_completa[18];
	   num[8] = ENDSTR;
	//   return num;
	}

};

#endif // #ifndef __Util_h_
