/*
-----------------------------------------------------------------------------
Filename:    DeviceDriver.h
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
*/
#ifndef __DeviceDriver_h_
#define __DeviceDriver_h_

#include "StdAfx.h"
// tarjeta de adquisici�n
#include "driver.h"

//ADQUISICION DE DATOS, DEFINICIONES NECESARIAS
#define     MAX_DEVICES			100
static      DEVFEATURES			DevFeatures;    // structure for device features
static		PT_DioReadPortByte	byteDigitalLeido;
static		PT_AOVoltageOut		lpAOVoltageOut;

using namespace Ogre;
/**
* Gesti�n de funcionalidad de la tarjeta de adquisici�n de datos modelo Advantech USB 4711A
*
* Ofrece una API sencilla para la inicializaci�n y liberaci�n del hardware, para la emisi�n de pulsos anal�gicos y la lectura conjunta de los 8 bits digitales (como un �nico byte)
*
* @author Gabriel Sanmart�n D�az
*/
class DeviceDriver
{
	public:
		DeviceDriver();
		~DeviceDriver();
		void init();
		void close();
		LRESULT emitPulse(int chanNumber, float voltage);
		USHORT readByte(USHORT &value);

	private:

		char        szErrMsg[80];               // Use for MESSAGEBOX function
		LRESULT     ErrCde;                     // Return error code
		//
		// used for fixed memory for the installed devices
		//
		DEVLIST     DeviceList[MAX_DEVICES];
		DEVLIST     SubDeviceList[MAX_DEVICES];
		LONG        DriverHandle;          // driver handle
		BOOL        bRun;                       // flag for running
		USHORT      gwDevice, gwSubDevice;      // Device index
		USHORT      gwChannel;                      // input channel
		USHORT      gwValue;                            // input value

		USHORT      gwGain;                      // gain index
		SHORT       gnNumOfDevices, gnNumOfSubdevices;  // number of installed devices

		bool isOpen;
};


#endif // #ifndef __DeviceDriver_h_