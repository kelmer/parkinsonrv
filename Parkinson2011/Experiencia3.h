/*
-----------------------------------------------------------------------------
Filename:    Experiencia3.h
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
*/
#ifndef __Experiencia3_h_
#define __Experiencia3_h_

#include "StdAfx.h"
#include "Experiencia.h"

/**
* Define el comportamiento de la Experiencia3 (marcha sobre treadmill).
*
* @todo Transferir l�gica completa de versi�n anterior
*
* @author Gabriel Sanmart�n D�az
*/
class Experiencia3 : public Experiencia
{
public:
    Experiencia3(void);
    virtual ~Experiencia3(void);

protected:
	
     void createScene(void);

};

#endif // #ifndef __Experiencia3_h_