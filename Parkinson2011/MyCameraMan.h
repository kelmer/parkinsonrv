/*
-----------------------------------------------------------------------------
Filename:    MyCameraMan.h
-----------------------------------------------------------------------------
*/
#ifndef __MyCameraMan_h_
#define __MyCameraMan_h_

#include "StdAfx.h"
#include <SdkTrays.h>
#include <SdkCameraMan.h>

class MyCameraMan : public OgreBites::SdkCameraMan
{
	protected:
		int		multMouseY;
		int		multMouseX;
		bool	applyMultiplier;
	public:
		MyCameraMan(Ogre::Camera* cam);
		virtual void injectKeyUp(const OIS::KeyEvent& evt);
		virtual void injectKeyDown(const OIS::KeyEvent& evt);
		virtual void injectMouseMove(const OIS::MouseEvent& evt);
		inline void setMouseMultX(int m){ multMouseX = m; };
		inline void setMouseMultY(int m){ multMouseY = m; };
		inline void setApplyMultiplier(bool apply) { applyMultiplier = apply; };
		inline bool getApplyMultiplier() { return applyMultiplier; };

};

#endif // #ifndef __MyCameraMan_h_