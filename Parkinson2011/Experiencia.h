#ifndef __Experiencia_h_
#define __Experiencia_h_

#include "StdAfx.h"
#include "BaseApplication.h"
#include "Personaje.h"
#include "FileWriter.h"
#include "DeviceDriver.h"
#include "J_Enviar.h"
#include "J_Recibir.h"
#include "HiResTimer.h"

/**
* Base para la definici�n de experiencias.
*
* TODO el proyecto ha sido creado de cero para objetificarlo ya que antes era
* programaci�n estructurada en un solo fichero, y por tanto, un caos.
*
* Esta clase representa los aspectos comunes a todas las experiencias, que son
* MUCHOS m�s de los que se podr�a pensar, esto incluye la creaci�n de la escena,
* la creaci�n de c�mara, el acceso al teclado, la carga de un escenario y un
* personaje...
*
* @author Gabriel Sanmart�n D�az
*/
class Experiencia : public BaseApplication
{
public:
	Experiencia(int	protocolos, int dataPerProtocol, int expData);
	virtual ~Experiencia(void);

protected:

	/* ATRIBUTOS Y PROPIEDADES COMUNES A TODAS LAS EXPERIENCIAS*/

	//Aunque la exp3 puede tener varias entities, habr� un nodo comun
	Ogre::Entity*				entScenario;			///< Entidad para el escenario 
	Ogre::SceneNode*			nodeScenario;			///< Nodo para el escenario o decorado de la escena para cada experiencia. @todo adaptar �sto para la Experiencia3
	Ogre::NameValuePairList		mInfo;					///< Realmente esto no sirve para nada

	//Node para la c�mara principal
	Ogre::SceneNode*			nodeCamera;				///< Nodo de la c�mara principal
	int							mMove;					///< Velocidad de desplazamiento de la c�mara @see MyCameraMan
	

	/** 
	* Posici�n inicial de la c�mara.
	* Almacena la posici�n inicial de la c�mara, para poder restaurarla posteriormente con F1 y para hacer la interpolaci�n de la c�mara invertida. 
	* @see finalPosCamera
	*/
	 Vector3			initialPosCamera;
	 
	/** 
	* Orientaci�n inicial de la c�mara.
	* Almacena la orientaci�n inicial de la c�mara, para poder restaurarla posteriormente con F1 y para hacer la interpolaci�n de la c�mara invertida. 
	* @see finalOrientationCamera
	*/
	 Quaternion			initialOrientation;		//orientaci�n inicial

	/** 
	* Multiplicador en X para el mouse.
	*
	* Para el uso de las gafas de RV (el HMD), �stas trackean el movimiento de la cabeza y lo asocian al movimiento de un rat�n. 
	* Sin embargo por defecto este movimiento es muy lento as� que lo que hacemos es aumentar la "sensibilidad" del rat�n a�adiendo un multiplicador al desplazamietno de la c�mara.
	*/
	int							multX;
	/** 
	* Multiplicador en Y para el mouse.
	*
	* Para el uso de las gafas de RV (el HMD), �stas trackean el movimiento de la cabeza y lo asocian al movimiento de un rat�n. 
	* Sin embargo por defecto este movimiento es muy lento as� que lo que hacemos es aumentar la "sensibilidad" del rat�n a�adiendo un multiplicador al desplazamietno de la c�mara.
	*/
	int							multY;


	/**
	*
	* Variables requeridas para poner la pantalla en negro
	*
	*/
	Entity*						entNegro;				///< Entidad para el plano que se emplea para poner la pantalla en negro al pulsar V.
	SceneNode*					nodeNegro;				///< Nodo del plano que se emplea para poner la pantalla en negro al pulsar la tecla V.
	bool						blackSwitch;			///< Switch para "apagar o encender la luz".


	Personaje*					personaje; ///< Todas las experiencias tienen un personaje. @see Personaje


	/** @name Descriptores de archivos
	* Archivos, gestores de los diferentes archivos.
	* @see FileWriter
	*/
	//@{
	FileWriter					file1; ///< Archivo principal, empleado por todas las experiencias.
	FileWriter					file2; ///< Archivo secundario, utilizado como apoyo para separar la informaci�n en alg�n momento puntual en que haya sido requerido de dicha forma.
	FileWriter					fileEventoA; ///< Archivo que almacena cualquier evento recibido en el canal adicional de la TAD, reservado para ello.
	FileWriter					fileEventoB; ///< Archivo que almacena cualquier evento recibido en el canal adicional de la TAD, reservado para ello.
	//@}


	DeviceDriver				pulseController; ///< Controlador de la tarj. adquisici�n, que permite la adquisici�n de informaci�n y la emisi�n de pulsos. 



	//Variables y declaraciones del calibrado
	bool						calibrationDone;

	/** @name Sockets de entrada y salida
	* Variables para el manejo y control de los sockets de recepci�n y env�o a y desde las c�maras y la aplicaci�n de control
	*/
	//@{
	/** 
	* Indica el puerto de entrada para la recepci�n de la informaci�n desde la aplicaci�n de control de las c�maras.
	* Este puerto es com�n a todas las experiencias y se ha fijado por defecto en el 3305. No necesita ser reimplementado.
	*/
	int							inputPortCam;
	sockets::J_Recibir*			receiverCam;	 //Un receptor para la informaci�n enviada por la camara 4

	/** 
	* Indica el puerto de entrada para la recepci�n de la informaci�n desde la aplicaci�n o interfaz de control de las experiencias.
	* Este puerto var�a seg�n la experiencia, estableci�ndose los puertos 402X donde X indica el n�mero de experiencia. Debe especificarse el n�mero de puerto antes de la inicializaci�n de sockets.
	*/
	int							inputPortGUI;
	sockets::J_Recibir*			receiverGUI;	  ///< Y otro receptor para la inforamci�n enviada por la GUI

	/** 
	* Indica el puerto de salida para el env�o de datos en caso de ser necesario, por ejemplo para el env�o del estado actual de la ejecuci�n y el protocolo en activo.
	* El puerto de salida se ha determinado como el mismo para todas las experiencias: 5001
	*/
	int							outputPort;		  ///< Por �ltimo el puerto de salida
	sockets::J_Enviar*			sender;			  ///< El tercer socket es por si necesitamos enviar informaci�n (como p. ej. en la exp1)

	/*
	Buffers para el almacenamiento de los datos recogidos por cada socket. Buffer de datos posicionales (c�maras) y de datos de control (GUI)
	*/
	float						datosCamaras[150];///< Buffer para valores que voy a leer desde el socket receptor (informaci�n que viene desde las c�maras).
	float*						datosGUI;	  ///< Buffer para albergar los par�metros enviados desde la GUI


	int							nProtocolos;  ///< N�mero de protocolos para la experiencia actual
	int							nDataPerProtocol; ///< N�mero de datos recibidos por GUI para el control por protocolo
	int							nExpData;			///< N�mero de datos gen�ricos de la experiencia (no espec�ficos de protocolo).
	int							nDatosGUI;		///< N�mero de datos que se recibir�n de la GUI. La suma es nExpData + nDataPerProtocol*nProtocolos;
	bool						recepcionOK;	  ///< Flag que indica si la �ltima recepci�n se ha realizado correctamente. Necesario para saber si actualizar o no las variables del programa (si no se ha recibido podr�an ser valores basura)
	//@}

	//Debug panel
	/**
	* Panel de debug situado en la esquina superior izquierda
	* Para introducir elementos en el panel e imprimirlos en pantalla, debe introducirse de forma secuencial primero un t�tulo para el elemento a imprimir en mDebugLineNames y posteriormente el valor del elemento en mDebugLineValues. Las funciones printLine simplifican y automatizan este proceso.
	* @see mDebugLineNames
	* @see mDebugLineValues
	* @see printLine
	*/
	OgreBites::ParamsPanel*		mDebugPanel;
	Ogre::StringVector			mDebugLineNames;	///< Vector de strings que contiene los t�tulos descriptivos de las l�neas de debug
	Ogre::StringVector			mDebugLineValues;	///< Vector de strings que contiene los valores de las variables a imprimir por pantalla en el panel de debug

	/** @name Variables temporales
	* Controlan los temporizadores para los timestamps a imprimir en los distintos archivos de registro
	*/
	//@{
	clock_t						tiempoInicial;	///< Tiempo de inicio del programa, para comparar con el resto de valores
	clock_t						tiempoActual;	///< Medici�n de tiempo en cada instante con respecto al inicio del programa
	float						secs;			///< Segundos transcurridos
	float						tiempoTotal;	///< tiempo total transcurrido?
	CHiResTimer					timer;			///< Variable que lleva de forma efectiva el conteo de tiempo
	//@}



	/** @name Variables de flujo de protocolos
	* Controlan el flujo de la aplicaci�n y su divisi�n en protocolos
	*/
	//@{

	/** 
	* Indica si la ejecuci�n de la experiencia se ha iniciado o no.
	* Com�n a todas las experiencias, indica en todo momento el estado de ejecuci�n de la misma. Su valor ser� cambiado a trav�s de la aplicaci�n externa de control.
	*/
	bool						started;

	//Variables relativas a los protocolos
	bool						random; ///< Indica si la ejecuci�n de los protocolos se efectuar� de forma aleatoria o no. 
	bool						randomized;					///< Indicar si ya se ha realizado la randomizaci�n, para no repetirlo en el siguiente ciclo. @see random

	bool						resetExp; ///< Flag para indicar si la experiencia ha de resetearse en el siguiente ciclo de render.
	bool						reiniciarProtocolos;		///< Esta variable determina si debe reiniciarse el conteo de protocolos

	/**
	* �Es �sta la primera vez que ejecutamos el protocolo?
	* Permite conocer si es el primer ciclo de render para el protocolo actual, en cuyo caso el flag se pasa a false. Necesario 
	* para la impresi�n en el fichero de los par�metros del protocolo configurados por el investigador (de otro modo se imprimir�a
	* en cada ciclo.
	*/
	bool						primeraVezProtocolo; ///< Indica si es la primera vez que se ejecuta el protocolo.
	/**
	* Array de ejecuci�n de protocolos.
	* Almacena los �ndices de los protocolos en el orden que deben ejecutarse. Por defecto se inicializa a 0 = 0, 1 = 1, 2 = 2,
	* etc. Siempre se sigue el orden especificado, de esta forma para randomizar la experiencia simplemente se aleatorizan
	* el orden de los �ndices almacenados en este array, sin repetici�n.
	**/
	std::vector<int>			ordenProtocolos;
	int							indice_protocolo_random;
	int							indice_protocolo_actual;    ///< �ndice del protocolo en el que nos hallamos actualmente

	/**
	* Tiempo que lleva el protocolo actual en ejecuci�n.
	* Es necesaria su medida para el caso de emplear tracking, ya que el fin del protocolo en este caso viene marcado 
	* por la duraci�n en segundos del protocolo y no por los ciclos de animaci�n.
	*/
	float							t_protocolo;

	 /**
	 * Ciclos de animaci�n transcurridos en el protocolo actual.
	 * Para el caso de estar activa la animaci�n y no el tracking, el final del protocolo actual viene marcado por el n�mero
	 * de ciclos especificado por el investigador en el GUI externo.
	 */	
	 int						numCiclosTranscurridos;

	 /**
	 * N�mero de ciclos que deben transcurrir antes de la emisi�n de un pulso de 5v.
	 * En ciertas circunstancias se requiri� la emisi�n desde el TAD de un pulso de 5v para el control del investigador de 
	 * ciertas condiciones de los experimentos. Por deficiencias en su hardware se hizo necesario espaciar la emisi�n de 
	 * dichos pulsos. Este n�mero (�nico para todos los protocolos) indica el n�mero de ciclos de animaci�n o de tracking 
	 * (tiempo de tracking) antes de la siguiente emisi�n de un pulso. Se configura desde el GUI externo y por defecto su
	 * valor es 0.
	 */
	 int						cyclesBtwnPulse; 

	/**
	 * N�mero de ciclos que se ha esperado para la emisi�n del siguiente pulso seg�n lo indicado en cyclesBtwnPulse
	 * Cuando se han de emitir pulsos de 5v (para su recepci�n por otros dispositivos) en ocasioens se desea que no se emitan en todos los ciclos,
	 * si no peri�dicamente cada cierto n�mero de ellos. La variable lleva el conteo de los ciclos que se han esperado sin emitir pulso y se 
	 * resetea con la emisi�n de �ste.
	 */	 
	 int						numCiclosEsperados;
	 //@}


	/**
	* Obtiene los milisegundos transcurridos desde el inicio del programa
	* @return numero de milisegundos transcurridos desde el inicio del programa
	*/
	int							getEllapsedMillisecs();

	/**
	* Lazo de render, base central de la l�gica de la aplicaci�n. Llama en orden a las distintas partes de ejecuci�n de un frame.
	*
	* @see calibrate()
	* @see executeExperienceFrame()
	* @see executeProtocol()
	* @see solveIK()
	*/
	bool						frameRenderingQueued(const FrameEvent& evt);


	/**
	* A llamar al cerrase la aplicaci�n, deber�a reimplementarse para cerrar y eliminar posibles punteros.
	*/
	void						destroyScene(void);

	//INICIALIZADORES
	/**
	* Crea la escena 3D para cada una de las experiencias antes de iniciarse el renderizado.
	* El esqueleto de createScene llama a los dem�s inicializadores de escena, aun cuando no est�n definidos en esta clase. 
	* Se producen tambi�n la inicializador del emisor de pulsos, del subsistema de sockets y del temporizador
	* El orden de ejecuci�n es el siguiente:
	*
	* @see createScenario()
	* @see createPersonaje()
	* @see createLights()
	* @see initAnimations()
	* @see createDebugPanel()
	* @see initSockets()
	* @see initCommonParameters()
	*/
	virtual	void				createScene(void);
	/**
	* Funci�n virtual para la carga en cada experiencia de los elementos del escenario (el "decorado").
	* Esta funcionalidad es com�n a las 3 experiencias.
	*/
	virtual void				createScenario(void) = 0;	//Escenario
	/**
	* Funcion virtual para la carga del Personaje.
	*/
	virtual void				createPersonaje(void) = 0;	//Personaje
	/**
	* Funcion virtual para la inicializaci�n de las luces de la escena.
	*/
	virtual void				createLights(void) = 0;		//Iluminaci�n
	/**
	* Funci�n virtual para la inicializaci�n de animaciones del personaje.
	* Todas las experiencias tienen en mayor o menor medida animaciones para simular los movimientos requeridos al usuario/paciente. La inicializaci�n se hace en esta funci�n.
	* Obviamente es necesario cargar previamente un personaje v�lido.
	* @see Personaje
	*/
	virtual void				initAnimations(void) = 0;	//Animaciones
	
	/**
	* Pone la pantalla en negro.
	* Dispone la pantalla en negro, superponiendo un plano negro frente a la c�mara e impidiendo el movimiento con el rat�n.
	*
	*/
	void						setBlankScreen(bool);
	/**
	* Inicializa la c�mara principal de la escena 3D.
	* Crea la c�mara de la escena y le asigna la posici�n inicial en el origen de coordenadas. Posteriormente asigna la c�mara a un nodo de la escena para simplificar su transformaci�n.
	* Crea un plano sobre la c�mara con un material negro para simular el "apagado de luces" al pulsar la tecla V sobre todas las experiencias.
	* @see mCameraMan
	* @see entNegro
	* @see nodeNegro
	* @see blackSwitch
	*/
	void						createCamera(void);			//C�mara
	/**
	* Re�ne la inicializaci�n de par�metros de renderizado base, comunes a las 3 experiencias (ventana, sombras, luz ambiente).
	* Probablemente no es necesaria la reimplementaci�n.
	*/
	void						initCommonParameters(void); //Par�metros de render
	/**
	* Inicializaci�n de los managers de sockets.
	* Asigna los puertos de entrada y salida para el socket receptor de informaci�n de las c�maras, el receptor de la GUI de control, y el socket emisor.
	* @see inputPortCam
	* @see inputPortGUI
	* @see outputPort
	*/
	void						initSockets(void);			//inicializa los sockets
	/**
	* Crea un panel adicional en la esquina superior izquierda en el que a�adir informaci�n de debug.
	* Para agregar l�neas de debug (titulo + linea), basta con utilizar los vectores mDebugLineNames y mDebugLineValues
	* @see mDebugPanel
	*/
	void						createDebugPanel(void);		//inicializa el panel de debug

	/**
	* Controla la pulsaci�n de teclas comunes a las experiencias (al pulsar).
	* Reimplementar con llamada super para a�adir funcionalidad.
	*/
	bool						keyPressed( const OIS::KeyEvent &arg );
	/**
	* Controla la pulsaci�n de teclas comunes a las experiencias (al soltar).
	* Reimplementar con llamada super para a�adir funcionalidad.
	*/
	bool						keyReleased( const OIS::KeyEvent &arg );

	/**
	* Actualiza los valores recogidos por los sockets de recepcion.
	* Ejecuta la sincronizaci�n de receiverCam y receiverGUI. La l�gica es com�n a todas las experiencias, no deber�a necesitar reimplementaci�n. Los datos se almacenan en datosGUI y datosCamaras.
	*/
	void						updateSockets(void);
	/**
	* Funci�n virtual a implementar por cada experiencia que actualiza los par�metros de la l�gica de �sta.
	* Actualiza, a partir de los buffers datosCamaras y datosGUI, las variables pertinentes de cada experiencia. Espec�fica a cada implementaci�n.
	*/
	virtual void				updateVars(void) = 0;
	

	//Divisi�n de la ejecuci�n de la experiencia
	/**
	*
	* Realiza la calibraci�n si la hubiere.
	* Primer paso de calibraci�n para el caso en que �sta sea necesaria, por ejemplo para tomar datos de referencia y establecer a partir de ellos
	* datos posicionales relativos. Dejar vac�a si no es necesaria la calibraci�n. Se pasa a la siguiente fase pulsando espacio.
	*
	*/
	virtual void				calibrate() = 0;
	/**
	*
	* Avanza un frame la ejecuci�n de la experiencia.
	* Realiza la l�gica de reordenamiento y flujo de control de ejecuci�n de protocolos as� como la emisi�n y recepci�n de pulsos a partir de la tarj. adquisici�n.
	* LLama a executeProtocol tras establecer el flujo de ejecuci�n.
	* @see executeProtocol()
	*/
	virtual void				executeExperienceFrame(const FrameEvent& evt) = 0;

	/*
	* Ejecuta una instancia del protocolo que corresponda o avanza en un frame la ejecuci�n de �ste.
	* Realiza las tareas l�gicas propias de la experiencia. i.e. es responsable de la prueba propiamente dicha.
	* Si fuere necesario, ejecuta solveIK para resolver problemas de cinem�tica inversa durante el tracking.
	* @see solveIK
	*/
	virtual void				executeProtocol(const FrameEvent& evt) = 0;
	/*
	* Realiza las tareas pertinentes a la cinem�tica inversa.
	* Soluciona la cinem�tica inversa para los miembros de la cadena de articulacions no involucrados en el tracking (brazos, piernas...)
	*
	*/
	virtual void				solveIK(const FrameEvent& evt) = 0;

	
	/**
	* Resetea el panel de debug, eliminando cualquier l�nea a mostrar.
	* Elimina los elementos de los vectores mDebugLineNames y mDebugLineValues, e imprime la l�nea de muestreo de tiempo transcurrido (necesaria en todas las experiencias). Ideal para llamarse al inicio del lazo de render. 
	*/
	void						clearDebug();
	//para imprimir lineas de debug, a�adimos para tipos basicos.

	/** @name Funciones printLine
	* Facilitan la impresi�n de l�neas de debug por pantalla.
	*
	* Las distintas sobrecargas permiten la impresi�n de una l�nea de descripci�n primero y un dato posterior (otro string, un float, un Vector...), en el panel propio de debug (esquina superior izquierda).
	* @param title T�tulo descriptivo del valor a imprimir
	* @param value Valor a imprimir
	*/
	//@{
	void						printLine(String title, String value);
	void						printLine(String title, float value);
	void						printLine(String title, int value);
	void						printLine(String title, Vector3 value);
	/**
	* No se ofrece impresi�n en radianes por ofrecer valores sin significado simple. Convertir primero en Degrees usando el constructor de �stos.
	*/
	void						printLine(String title, Degree value);
	void						printLine(String title, bool value);
	void						printLine(String title, char *value);
	//@}
};

#endif // #ifndef __Experiencia_h_
