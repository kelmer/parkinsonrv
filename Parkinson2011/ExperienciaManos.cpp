/*
-----------------------------------------------------------------------------
Filename:    ExperienciaManos.cpp
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
*/
#include	"StdAfx.h"
#include	"ExperienciaManos.h"
#define		DIANA_DERECHA 9
#define		DIANA_IZQUIERDA 10
//-------------------------------------------------------------------------------------
ExperienciaManos::ExperienciaManos(int	protocolos, int dataPerProtocol, int expData):Experiencia(protocolos, dataPerProtocol, expData),
									showIKPoints(false),
									posManoDebug(false)
{	

}
//-------------------------------------------------------------------------------------
ExperienciaManos::~ExperienciaManos(void)
{
}

void ExperienciaManos::createScenario(void)
{
	//Creamos el escenario, es com�n para las experiencias 1 y 2
	entScenario = mSceneMgr->createEntity("Habitacion", "habita.mesh");
			entScenario->setCastShadows(false); //que no provoquen sombras
			entScenario->getSubEntity(DIANA_DERECHA)->setVisible(false); //fuera las dianas
			entScenario->getSubEntity(DIANA_IZQUIERDA)->setVisible(false);
	nodeScenario = mSceneMgr->getRootSceneNode()->createChildSceneNode("habitacionNode");
			nodeScenario->attachObject(entScenario);
	
}

void ExperienciaManos::createPersonaje(void)
{
	personaje = new Personaje(mSceneMgr, "Personaje", "PersonajeNode", "personaxe.mesh");
	personaje->setPosition(Vector3(0.0f, -3.80f, 29.773f)-Vector3(0.0f,4.2f,0.0f));
}

void ExperienciaManos::createLights(void)
{
	/*LUZ 4 DE FRENTE PARA PARED DE LA ESPALDA*/
	{
		
		//LUZ 4 de frente para pared de la espalda
		Light *light = mSceneMgr->createLight("mainLight");
		light->setType(Light::LT_DIRECTIONAL);
		light->setPosition(Vector3(0, 0, 0));
		light->setDirection(0,-1,0);
		light->setDiffuseColour(Ogre::ColourValue(0.8f, 0.8f, 0.8f));
		light->setSpecularColour(Ogre::ColourValue(0.5f, 0.5f, 0.5f));
		light->setCastShadows(true);



		//LUZ 4 de frente para pared de la espalda
		//Light *light4 = mSceneMgr->createLight("mainLight");
		//light4->setType(Light::LT_POINT);
		////light4->setPosition(Vector3(0, 150, -300));
		//light4->setPosition(Vector3(0, 23, 0));
		//light4->setDiffuseColour(ColourValue::White);
		//light4->setSpecularColour(ColourValue::White);
		//light4->setCastShadows(true);
		

	}
}

void ExperienciaManos::initAnimations(void)
{
	//Movimiento del dedo derecho, debe durar 1 seg la animacion
	as_dedeoDer			= personaje->getAnimation("dedeoDereito");
	//as_dedeoDer			= personaje->getAnimation("dedeoMedioDto");
	as_dedeoDer->setLength(1);
	as_dedeoDer->setLoop(false);

	as_dedeoEsq			= personaje->getAnimation("dedeoEsquerdo");
	as_dedeoEsq->setLength(1);
	as_dedeoEsq->setLoop(false);


	//�stas en cambio duran 1 segundo para ir, y otro para volver
	as_dianaRectoDta	= personaje->getAnimation("dianaRectoDta");
	as_dianaRectoDta->setLength(2);

	as_dianaCruceDta	= personaje->getAnimation("dianaCruceDta");
	as_dianaCruceDta->setLength(2);

	as_dianaRectoEsq	= personaje->getAnimation("dianaRectoEsq");
	as_dianaRectoEsq->setLength(2);

	as_dianaCruceEsq	= personaje->getAnimation("dianaCruceEsq");
	as_dianaCruceEsq->setLength(2);
}

void ExperienciaManos::createIKElements()
{
	//Creamos los elementos de Cinem�tica inversa, en este caso son 
	personaje->addIKElement(new IKElement("dedoDerecho"));
	personaje->addIKElement(new IKElement("dedoIzquierdo"));
	personaje->addIKElement(new IKElement("brazoDerecho"));
	personaje->addIKElement(new IKElement("brazoIzquierdo"));

	//Creamos un DOF, como no lo pasamos por referencia puedo usar un �nico objeto, o eso creo.
	DOF_constraint dof;
	//Dedo derecho
	{
		dof.max_x = 0;
		dof.min_x = 0;
		dof.max_y = -10;
		dof.min_y = -50;
		dof.max_z = 50;
		dof.min_z = -20;
		personaje->addBoneToIKElement("dedoDerecho", "Dedo2Der01", dof);


		dof.max_x = 0;
		dof.min_x = 0;
		dof.max_y = 0;
		dof.min_y = 0;
		dof.max_z = 0;
		dof.min_z = -90;
		personaje->addBoneToIKElement("dedoDerecho", "Dedo2Der02", dof);
		personaje->addBoneToIKElement("dedoDerecho", "Dedo2Der03", dof);
		personaje->addBoneToIKElement("dedoDerecho", "Dedo2Der04", dof);
		personaje->getIKElement("dedoDerecho")->setEffector(3);
		personaje->getIKElement("dedoDerecho")->setFirstBone(0);
	}
	//Dedo izquierdo
	{
		dof.max_x = 0;
		dof.min_x = 0;
		dof.max_y = -35;
		dof.min_y = -70;
		dof.max_z = 0;
		dof.min_z = 0;
		personaje->addBoneToIKElement("dedoIzquierdo", "Dedo2Esq01", dof);


		dof.max_x = 0;
		dof.min_x = 0;
		dof.max_y = 0;
		dof.min_y = 0;
		dof.max_z = 0;
		dof.min_z = -90;
		personaje->addBoneToIKElement("dedoIzquierdo", "Dedo2Esq02", dof);
		personaje->addBoneToIKElement("dedoIzquierdo", "Dedo2Esq03", dof);
		personaje->addBoneToIKElement("dedoIzquierdo", "Dedo2Esq04", dof);
		personaje->getIKElement("dedoIzquierdo")->setEffector(3);
		personaje->getIKElement("dedoIzquierdo")->setFirstBone(0);
	}
	//Brazo derecho
	{
		//x
		dof.max_x = 40;
		dof.min_x = -30;
		//y
		dof.max_y = 0;
		dof.min_y = 0;
		//z
		dof.max_z = -10;
		dof.min_z = -175;
		personaje->addBoneToIKElement("brazoDerecho", "HombroDer", dof);
		//x
		dof.max_x = 0;
		dof.min_x = 0;
		//y
		dof.max_y = 50;
		dof.min_y = -18;
		//z
		dof.max_z = 0;
		dof.min_z = 0;
		personaje->addBoneToIKElement("brazoDerecho", "CodoDer", dof);
		personaje->addBoneToIKElement("brazoDerecho", "ManDer", dof);

		personaje->getIKElement("brazoDerecho")->setEffector(2);
		personaje->getIKElement("brazoDerecho")->setFirstBone(0);
	}
	//Brazo izquierdo
	{

		//x
		dof.max_x = 40;
		dof.min_x = -30;
		//y
		dof.max_y = 0;
		dof.min_y = 0;
		//z
		dof.max_z = -10;
		dof.min_z = -175;
		personaje->addBoneToIKElement("brazoIzquierdo", "HombroEsq", dof);
		//x
		dof.max_x = 0;
		dof.min_x = 0;
		//y
		dof.max_y = 18;
		dof.min_y = -50;
		//z
		dof.max_z = 0;
		dof.min_z = 0;
		personaje->addBoneToIKElement("brazoIzquierdo", "CodoEsq", dof);
		personaje->addBoneToIKElement("brazoIzquierdo", "ManEsq", dof);

		personaje->getIKElement("brazoIzquierdo")->setEffector(2);
		personaje->getIKElement("brazoIzquierdo")->setFirstBone(0);
	}
	

	
	//creamos los objetos de debug de IK
	puntomanoDer = mSceneMgr->createEntity("esferaManDer", Ogre::SceneManager::PT_SPHERE);
	puntoManoDerNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("PtoManoDerNode", posIKManoDerecha);
	puntoManoDerNode->attachObject(puntomanoDer);
	puntoManoDerNode->scale(0.002f,0.002f,0.002f);
	puntomanoIzq = mSceneMgr->createEntity("esferaManIzq", Ogre::SceneManager::PT_SPHERE);
	puntoManoIzqNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("PtoManoIzNode", posIKManoIzquierda);
	puntoManoIzqNode->attachObject(puntomanoIzq);
	puntoManoIzqNode->scale(0.002f,0.002f,0.002f);
	puntodedoDer = mSceneMgr->createEntity("esferaDedoDer", Ogre::SceneManager::PT_SPHERE);
	puntoDedoDerNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("PtoDedoDerNode",posIKDedoDerecho);
	puntoDedoDerNode->attachObject(puntodedoDer);
	puntoDedoDerNode->scale(0.001f,0.001f,0.001f);
	puntodedoIzq = mSceneMgr->createEntity("esferaDedoIzq", Ogre::SceneManager::PT_SPHERE);
	puntoDedoIzqNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("PtoDedoIzNode",posIKDedoIzquierdo);
	puntoDedoIzqNode->attachObject(puntodedoIzq);
	puntoDedoIzqNode->scale(0.001f,0.001f,0.001f);
	
	//CUBO PARA VER LAS DELIMITAICONES
	IKlimitCubeDer = mSceneMgr->createManualObject("cube");
	IKlimitCubeDer->begin("cubeMaterial", RenderOperation::OT_LINE_STRIP);


	//IKlimitCubeDer->position( 1.31764, -6.4673, 26.5); //0
	//IKlimitCubeDer->position( 1.81764, -6.4673, 26.5); //1
	//IKlimitCubeDer->position( 1.81764, -6.4673, 25.5); //2
	//IKlimitCubeDer->position( 1.31764, -6.4673, 25.5); //3

	//IKlimitCubeDer->position( 1.31764, -7.51676, 26.5); //4
	//IKlimitCubeDer->position( 1.31764, -7.51676, 25.5); //5
	//IKlimitCubeDer->position( 1.81764, -7.51676, 25.5);  //6
	//IKlimitCubeDer->position( 1.81764, -7.51676, 26.5);  //7

	
	IKlimitCubeDer->position( -0.25f, -0.575f, 1.0f); //0
	IKlimitCubeDer->position( 0.25f, -0.575f, 1.0f); //1
	IKlimitCubeDer->position( 0.25f, -0.575f, 0.0f); //2
	IKlimitCubeDer->position( -0.25f, -0.575f, 0.0f); //3

	IKlimitCubeDer->position( -0.25f, 0.575f, 1.0f); //4
	IKlimitCubeDer->position( -0.25f, 0.575f, -0.0f); //5
	IKlimitCubeDer->position( 0.25f, 0.575f, -0.0f); //6
	IKlimitCubeDer->position( 0.25f, 0.575f, 1.0f); //7


	IKlimitCubeDer->index(0);
	IKlimitCubeDer->index(1);
	IKlimitCubeDer->index(2);
	IKlimitCubeDer->index(3);
	IKlimitCubeDer->index(0);
	IKlimitCubeDer->index(4);
	IKlimitCubeDer->index(5);
	IKlimitCubeDer->index(3);
	IKlimitCubeDer->index(5);
	IKlimitCubeDer->index(6);
	IKlimitCubeDer->index(2);
	IKlimitCubeDer->index(6);
	IKlimitCubeDer->index(7);
	IKlimitCubeDer->index(1);
	IKlimitCubeDer->index(7);
	IKlimitCubeDer->index(4);



	IKlimitCubeDer->end();

	IKCubeDerNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	IKCubeDerNode->attachObject(IKlimitCubeDer);


	//Creamos otro cubo para debug, luego borrar todo este codigo hasta el final de la funcion

	ManualObject * IKlimitCubeMano = mSceneMgr->createManualObject("cubeMano");
	IKlimitCubeMano->begin("cubeMaterial", RenderOperation::OT_LINE_STRIP);
	
	IKlimitCubeMano->position( -1.5f, -7.3544f, 28.0f); //0
	IKlimitCubeMano->position(	3.0f, -7.3544f, 28.0f); //1
	IKlimitCubeMano->position(  3.0f, -7.3544f, 24.324f); //2
	IKlimitCubeMano->position( -1.5f, -7.3544f, 24.324f); //3

	IKlimitCubeMano->position( -1.5f, -3.8544f, 28.0f); //4
	IKlimitCubeMano->position( -1.5f, -3.8544f, 24.324f); //5
	IKlimitCubeMano->position(  3.0f, -3.8544f, 24.324f); //6
	IKlimitCubeMano->position(  3.0f, -3.8544f, 28.0f); //7


	IKlimitCubeMano->index(0);
	IKlimitCubeMano->index(1);
	IKlimitCubeMano->index(2);
	IKlimitCubeMano->index(3);
	IKlimitCubeMano->index(0);
	IKlimitCubeMano->index(4);
	IKlimitCubeMano->index(5);
	IKlimitCubeMano->index(3);
	IKlimitCubeMano->index(5);
	IKlimitCubeMano->index(6);
	IKlimitCubeMano->index(2);
	IKlimitCubeMano->index(6);
	IKlimitCubeMano->index(7);
	IKlimitCubeMano->index(1);
	IKlimitCubeMano->index(7);
	IKlimitCubeMano->index(4);



	IKlimitCubeMano->end();
	IKlimitCubeMano->setVisible(false);

	SceneNode* IKCubeManoNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	IKCubeManoNode->attachObject(IKlimitCubeMano);

}

void ExperienciaManos::createScene(void)
{
	//Create scene se define en Experiencia, y llama a createScenario, createPersonaje, createLights, initAnimations
	Experiencia::createScene();

	createIKElements();


	/* FIN DEL INIT */
}