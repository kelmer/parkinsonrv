/*
-----------------------------------------------------------------------------
Filename:    DeviceDriver.cpp
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
*/
#include "StdAfx.h"
#include "DeviceDriver.h"

//-------------------------------------------------------------------------------------
DeviceDriver::DeviceDriver():	DriverHandle(NULL), 
								bRun(false), 
								gwDevice(0), 
								gwSubDevice(0),
								gwChannel(0),
								gwGain(0),
								isOpen(false)
{

}
//-------------------------------------------------------------------------------------
DeviceDriver::~DeviceDriver(void)
{
}


void DeviceDriver::init(void){
	ErrCde = DRV_DeviceOpen(gwDevice,(LONG far *)&DriverHandle);
	if(ErrCde == 0)
		isOpen = true;
}

void DeviceDriver::close(void){
	DRV_DeviceClose((LONG far *)&DriverHandle);
	isOpen = false;
}

LRESULT DeviceDriver::emitPulse(int chanNumber, float voltage){
	if(isOpen){
		lpAOVoltageOut.chan = chanNumber;
		lpAOVoltageOut.OutputValue = voltage;
	
		//Si hay error devolvemos el c�digo (lanzamos excepci�n para que la catchee el programa principal y haga debug)
		//Si es distinto de cero es que hay error
		return DRV_AOVoltageOut(DriverHandle,(LPT_AOVoltageOut)&lpAOVoltageOut);
	}
	else
		return -1;
}

//Lee los 8 bits de entrada digital y lo convierte en un byte, por eso el canal es indiferente
//Pasamos por referencia el valor leido para as� poder devolver c�digos de error
USHORT DeviceDriver::readByte(USHORT &value){
	if(isOpen){
		gwChannel = 0;    //Steer1

		byteDigitalLeido.port = gwChannel;
		byteDigitalLeido.value = (USHORT far *)&gwValue;

		//Si es distinto de cero, la cagamos
		ErrCde = DRV_DioReadPortByte(DriverHandle,(LPT_DioReadPortByte)&byteDigitalLeido);
    
		//los valores est�n en ALTA si no activos, por tanto invertimos para que rule el AND
		//~es el operador not biwtise
		gwValue=~gwValue;

		value = gwValue;
		return ErrCde;
	}
	else
		return -1;
}